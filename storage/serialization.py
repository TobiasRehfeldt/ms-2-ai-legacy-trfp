import os

import msgpack_numpy as np_msg


def write(obj, name):
    with open(name, 'wb') as file:
        return file.write(np_msg.packb(obj))


def read(name):
    with open(name, 'rb') as file:
        return np_msg.unpackb(file.read(), raw=False)


def __create_file(path):
    if not os.path.exists(path):
        os.makedirs(path)