import collections
from copy import deepcopy
from xml.etree import ElementTree as ET
from zipfile import ZipExtFile

import xmltodict

from pride_api.get_urls import __get_loose_files, __get_files_api
from storage.db_init import __insert_or_update, __get_client_dbs
from utils.common import __url_open_attempts


def __flatten_dict(init_dict, out_dict=None, parent_key=None, keep_parents=False, sep="/", desired_keys=None):
    out = dict() if out_dict is None else out_dict
    for k, v in init_dict.items():
        if isinstance(v, collections.MutableMapping):
            __flatten_dict(v, out_dict=out, parent_key=str(k), keep_parents=keep_parents, sep=sep, desired_keys=desired_keys)
        else:
            if k in out and not parent_key:
                continue
            k = parent_key + sep + str(k) if k in out or parent_key and keep_parents else parent_key if k == 'string' else k
            if desired_keys and k.split(sep)[-1] not in desired_keys:
                continue
            out[k] = v

    return out


def __xml_keys():
    return ['filePaths', 'fileNames', 'pvalThres', 'includeContaminants', 'matchBetweenRuns', 'ibaq', 'top3',
            'minPepLen', 'peptideFdr', 'proteinFdr', 'siteFdr', 'maxCharge', 'minPeakLen', 'enzymes',
            'fixedModifications', 'variableModifications', 'MatchTolerance', 'MatchToleranceInPpm',
            'DeisotopeTolerance', 'DeisotopeToleranceInPpm', 'fastaFilePaths', 'restrictMods']


def __loose_xmls(accnr):
    return [__url_open_attempts(f) for f in __get_loose_files(__get_files_api(accnr)) if f.endswith('xml')]


def __xml_parser(xml_files, accnr, rawfiles):
    if xml_files:
        try:
            # reads the xml elements from zip files
            xml_files = [f.read() if isinstance(f, ZipExtFile) else f for f in xml_files]

            # parse all xml files into dictionary format
            xml_dict = [dict(xmltodict.parse(f)) for f in xml_files if f]
            # Flatten the dictionary
            xml_dict = [__flatten_dict(f.get('MaxQuantParams'), desired_keys=__xml_keys()) for f in xml_dict]

            # If filePaths do not exist, set the fileNames to the filepaths. if fileNames do not exists set it to None and remove it
            [f.setdefault('filePaths', f.get('fileNames')) for f in xml_dict if not f.get('filePaths')]
            [xml_dict.remove(f) for f in deepcopy(xml_dict) if not f.get('filePaths')]
            [f.update({'filePaths': [g.replace('\\', '/').split('/')[-1].split('.')[0] for g in f.get('filePaths')]}) for f in xml_dict]

            # Create an identification dict and a info dict to later join together to make the complete info xml dict.
            xml_dict = [{**{'Raw file': g, 'accession': accnr, 'identifier': f'{accnr}/{g}', 'xml_exists': True}, **f} for f in xml_dict for g in f['filePaths']]
            [__insert_or_update(__get_client_dbs('ms2ai', 'files')[0], xml, ['identifier']) for xml in xml_dict]

        except Exception as e:
            error_input = [{'accession': accnr, 'Raw file': f.split('/')[-1], 'identifier': f'{accnr}/{f}', 'xml_exists': True, 'xml_error': e} for f in rawfiles]
            [__insert_or_update(__get_client_dbs('ms2ai', 'files')[0], xml, ['identifier']) for xml in error_input]
    elif not xml_files and rawfiles:
        missing_input = [{'accession': accnr, 'Raw file': f.split('/')[-1], 'identifier': f'{accnr}/{f}', 'xml_exists': False} for f in rawfiles]
        [__insert_or_update(__get_client_dbs('ms2ai', 'files')[0], xml, ['identifier']) for xml in missing_input]