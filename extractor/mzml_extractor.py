import gzip
import os
from math import ceil

from pyteomics import mzml

from extractor.common import __import_table
from storage.db_init import __get_client_dbs
from storage.serialization import write
from utils.config_inits import get_econfig, __remove_used
from utils.log import __internal_print


def __process_ms1(spectrum):
    # Scan information
    scan_info = spectrum['scanList']
    # Time
    scan_time = scan_info['scan'][0]['scan start time']
    # data arrays
    mz_array = spectrum['m/z array']
    int_array = spectrum['intensity array']

    return {'scan time': scan_time, 'intensity': int_array.tolist(), 'mz': mz_array.tolist()}


def __get_fragmentations(spectrum):
    # Fragmentation information
    frags = [f for i, f in enumerate(spectrum['precursorList']['precursor'][0]['activation']) if i % 2 == 0 or i == 1]
    fragmentation = {}

    for i, f in enumerate(frags):
        idx = int(i/2)+1
        frag_index = f"{idx}{'st' if str(idx).endswith('1') else 'nd' if str(idx).endswith('2') else 'rd' if str(idx).endswith('3') else 'th'}"
        if 'energy' in f:
            fragmentation[f'{frag_index} fragmentation energy'] = spectrum['precursorList']['precursor'][0]['activation'][f]
        else:
            fragmentation[f'{frag_index} fragmentation type'] = f

    return fragmentation


def __process_ms2(spectrum, ms2_index):
    # Retention time of scan
    scan_time = spectrum['scanList']['scan'][0]['scan start time']
    # Precursor information
    precursor_mz = spectrum['precursorList']['precursor'][0]['selectedIonList']['selectedIon'][0]['selected ion m/z']
    precursor_scan_id = spectrum['precursorList']['precursor'][0]['spectrumRef'].split('scan=')[1]
    mz_filter_min, mz_filter_max = spectrum['scanList']['scan'][0]['scanWindowList']['scanWindow'][0].values()
    fragmentation = __get_fragmentations(spectrum)

    # Get m/z and intensity arrays
    mz_array = spectrum['m/z array']
    int_array = spectrum['intensity array']

    return {'scan time': scan_time, 'm/z': mz_array, 'intensity': int_array, 'precursor scan': precursor_scan_id,
            'precursor m/z': precursor_mz, 'min m/z filter': mz_filter_min, 'max m/z filter': mz_filter_max, 'ms2 rt index': ms2_index}, fragmentation


def create_extraction_dict(data, filepath, multiprocessing, pep_rts):
    # Initialize Peptide-representation information
    mz_filter_min, mz_filter_max = data[0]['scanList']['scan'][0]['scanWindowList']['scanWindow'][0].values()

    # Initialize empty Run-representation dictionary
    extracted = {'ms1': {}, 'ms2': {}}
    ms1_index, ms2_index = (0, 0)
    for spectrum in data:
        scan = spectrum['index'] + 1
        if scan % ceil(len(data) / 20) == 0:
            __internal_print(f'Extracting data from mzML: {round(scan / len(data)*100)}%', multiprocessing)

        if spectrum['ms level'] == 1:
            try:
                # Info for Run-representations
                extracted['ms1'][str(scan)] = {**{'rt index': ms1_index}, **__process_ms1(spectrum)}
                ms1_index += 1
            except:
                continue

        elif spectrum['ms level'] == 2:
            try:
                # Info for Run-representations
                ms2info = __process_ms2(spectrum, ms2_index)
                extracted['ms2'][str(scan)] = {**ms2info[0], 'ms1 rt index': extracted['ms1'][ms2info[0]['precursor scan']]['rt index'], **ms2info[1]}
                ms2_index += 1
            except:
                continue

    ms1_vals = [[min(extracted['ms1'][f]['mz']), max(extracted['ms1'][f]['mz']), max(extracted['ms1'][f]['intensity'])] for f in extracted['ms1']]
    min_ms1_mz, max_ms1_mz, max_ms1_int = min([f[0] for f in ms1_vals]), max([f[1] for f in ms1_vals]), max([f[2] for f in ms1_vals])
    ms2_vals = [[min(extracted['ms2'][f]['m/z']), max(extracted['ms2'][f]['m/z']), max(extracted['ms2'][f]['intensity'])] for f in extracted['ms2']]
    min_ms2_mz, max_ms2_mz, max_ms2_int = min([f[0] for f in ms2_vals]), max([f[1] for f in ms2_vals]), max([f[2] for f in ms2_vals])

    extracted['file'] = {'min rt': data[0]['scanList']['scan'][0]['scan start time'],
                         'max rt': data[-1]['scanList']['scan'][0]['scan start time'],
                         'first pep rt': pep_rts[0], 'last pep rt': pep_rts[1],
                         'rt step size': data[-1]['scanList']['scan'][0]['scan start time'] / len(data),
                         'min mz filter': mz_filter_min, 'max mz filter': mz_filter_max,
                         'ms1': {'min m/z': min_ms1_mz, 'max m/z': max_ms1_mz, 'max intensity': max_ms1_int},
                         'ms2': {'min m/z': min_ms2_mz, 'max m/z': max_ms2_mz, 'max intensity': max_ms2_int}}

    write(extracted, f'{filepath}mzML.txt')


def __mzML_extractor(filepath, multiprocessing, accnr, filename):
    __internal_print('Extracting from formatted mzML file', multiprocessing)

    df = __import_table(f'{filepath}{get_econfig()["maxquant_file"]}')
    file_entry = __get_client_dbs('ms2ai', 'files')[0].find_one({'identifier': f'{accnr}/{filename}'})
    file_keys = ['min rt', 'max rt', 'rt step size', 'first pep rt', 'last pep rt', 'min mz filter', 'max mz filter']
    # Extract the data from the mzml, if we havnt already
    if (not os.path.exists(f'{filepath}mzML.txt') and os.path.exists(f'{filepath}file.mzML.gz')) or\
       (not all(item in file_entry for item in file_keys) and os.path.exists(f'{filepath}file.mzML.gz')):
        with gzip.open(f'{filepath}file.mzML.gz', 'rb') as gzipfile:
            __internal_print('Extracting data from mzML', multiprocessing)
            data = mzml.MzML(gzipfile)
            create_extraction_dict(data, filepath, multiprocessing, [min(df['Retention time']), max(df['Retention time'])])
        __remove_used(f'{filepath}file.mzML.gz')
