
def __arg_value(argument, db_entry, return_val):
    if argument in db_entry:
        return db_entry[argument]
    return return_val


def __one_hot(arg_ns, db_entry, index_dict, arg, default='other'):
    return [int(g == __arg_value(arg_ns, db_entry, default)) for g in index_dict[arg]]


def __linear_relationship(arg_ns, db_entry, index_dict, arg, default=0):
    return float(__arg_value(arg_ns, db_entry, default))


def __norm_value(arg_ns, db_entry, index_dict, arg, default=0):
    return float(__arg_value(arg_ns, db_entry, default) / index_dict[arg])