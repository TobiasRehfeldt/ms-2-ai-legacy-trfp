import os
import re
from collections import Counter
from functools import partial
from multiprocessing import Pool
from zipfile import ZipFile

from extractor.file_metadata import __mq_file_handler
from extractor.iter_zip import __local_iter
from pride_api.keys import __maxquant_file_names, __pride_keys
from storage.compressions import __ext_from_zip
from storage.db_init import __insert_or_update, __get_client_dbs
from utils.common import __validated_input
from utils.config_inits import get_econfig, __get_path, get_sconfig
from utils.log import ms2ai_log


def __loose_to_zip(dirpath):
    with ZipFile(f'{dirpath}working.zip', 'w') as zip_file:
        maxquant_files = [f for f in os.listdir(f'{dirpath}') if f in __maxquant_file_names() or f.endswith('.xml')]
        [zip_file.write(dirpath + f, f) for f in maxquant_files]
    zipfiles = [file for file in os.listdir(f'{dirpath}') if file.endswith('.zip')]
    return zipfiles


def __new_metadata(accnr):
    pride_keys = ['accession'] + __pride_keys()
    print('\nFor easy compatibility with other metadata, please provide exact string matching entries corresponding with the PRIDE standard values.\n'
          'Input values for the project below. Leave entry empty if non-applicable')
    manual_input = {}
    for f in pride_keys:
        key_input = input(f'Project {f if f != "accession" else "name (accession)"} ("?" for examples from other PRIDE projects)')
        if key_input == '?':
            while key_input == '?':
                print(', '.join([g[0] for g in Counter([g[f] for g in __get_client_dbs('ms2ai', 'projects')[0].find()]).most_common()][0:10]))
                key_input = input(f'Project {f if f != "accession" else "name (accession)"} ("?" for examples from other PRIDE projects)')
        else:
            manual_input[f] = None if key_input == '' and f != 'accession' else key_input

    if manual_input['accession'] == '':
        manual_input['accession'] = accnr
    manual_input['source'] = 'manual input'

    rename = True
    while __get_client_dbs('ms2ai', 'projects')[0].find_one({'accession': manual_input['accession']}) and rename:
        rename = __validated_input(f'Project name "{manual_input["accession"]}" already exists, overwrite or rename?',
                                   valid_values=['overwrite', 'rename'], equals_to='rename', default_option=None)
        if rename:
            manual_input['accession'] = input('Input new project name (accession): ')

    __insert_or_update(__get_client_dbs('ms2ai', 'projects')[0], manual_input, ['accession'])
    return manual_input['accession']


def __new_accnr(default_option='rename'):
    accnr = input('Input new project name: ')
    pre_existing = bool(__get_client_dbs('ms2ai', 'projects')[0].find_one({'accession': accnr}))
    while pre_existing:
        name_option = __validated_input(f'Directory with project name "{accnr}" already exists, use or rename?: ',
                                        valid_values=['use', 'rename'], equals_to='rename', default_option=default_option)
        if name_option:
            accnr = input('Input new project name: ')
            pre_existing = bool(__get_client_dbs('ms2ai', 'projects')[0].find_one({'accession': accnr}))
        else:
            pre_existing = False

    return accnr


def __metadata_handler(data_path, accnr):
    if data_path and __validated_input(f'Provide metadata for the project? (it is assumed that the informations provided apply to all raw files in the project) ',
                                       valid_values=['y', 'n'], equals_to='y', default_option='n'):
        accnr = __new_metadata(accnr)

    elif data_path and __validated_input(f'Choose a different project name than {accnr}? ',
                                         valid_values=['y', 'n'],  equals_to='y', default_option='n'):
        accnr = __new_accnr()

    return accnr


def __get_accnr(dirpath, zip_files, data_path):
    with ZipFile(f'{dirpath}{zip_files[0]}', 'r') as zip_file:
        mq_file = __ext_from_zip([get_econfig()['maxquant_file']], zip_file=zip_file)[0]
        df, files = __mq_file_handler(mq_file)
    if len(files) == 1 and files[0] != dirpath.split('/')[-2]:
        accnr = dirpath.split('/')[-2]
    else:
        accnr = dirpath.split('/')[-3]

    # Allow the user to provide metadata for the project, if the project is NOT in the project folder and already has information attached
    if not (__get_path() in dirpath and __get_client_dbs('ms2ai', 'projects')[0].find_one({'accession': accnr})) and data_path:
        accnr = __metadata_handler(data_path, accnr)

    return accnr


def local_peptide_extractor(dirpath, multiprocessing, data_path=False):
    # Find zipfiles or create a zipfile with the loose files in the project
    zip_files = [file for file in os.listdir(f'{dirpath}') if file.endswith('.zip')]
    if not zip_files and get_econfig()['maxquant_file'] in os.listdir(dirpath):
        zip_files = __loose_to_zip(dirpath)

    try:
        accnr = __get_accnr(dirpath, zip_files, data_path)
    except Exception as e:
        return

    if data_path and (get_sconfig()['multiprocessing']['processes'] and len(zip_files) > 1):
        with Pool(min(get_sconfig()['multiprocessing']['processes'], len(zip_files))) as ps:
            ps.map(partial(__local_iter, dirpath=dirpath, accnr=accnr, multiprocessing=True), zip_files)
    else:
        [__local_iter(f, dirpath, accnr, multiprocessing) for f in zip_files]
