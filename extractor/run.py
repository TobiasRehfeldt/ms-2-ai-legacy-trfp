import os
from functools import partial
from multiprocessing.pool import Pool

import numpy as np

from extractor.filters import __recreate_file_filter, __recreate_db_filter, __directory_filter
from extractor.only_metadata import __get_pride_project_metadata
from extractor.init_local import local_peptide_extractor
from extractor.init_pride import peptide_extractor
from extractor.reset_data import reset_data
from extractor.trfp import __prepare_formatting_software
from storage.db_init import __get_client_dbs, __insert_or_update
from utils.common import __validated_input
from utils.config_inits import __get_path, get_econfig, get_sconfig, get_nconfig


def __db_init(example):
    mz_bin, rt_bin = get_econfig()["mz_bin"], get_econfig()["rt_bin"]
    mz_interval, rt_interval = get_econfig()["mz_interval"], get_econfig()["rt_interval"]
    binning = [get_econfig()['ms2_binning']['apply'], get_econfig()['ms2_binning']['bin_amount']]
    pep_db = __get_client_dbs(None, 'peptides')[0]
    if '\\' in example:
        quit('All paths must be formatted using "/" instead of "\\"')

    if pep_db.find_one({'_id': 'sizes'}) and pep_db.find_one({'_id': {'$ne': 'sizes'}}) is not None:
        if list(pep_db.find_one({'_id': 'sizes'}).values())[1:] == [mz_bin, rt_bin, mz_interval, rt_interval, binning]:
            return
        else:
            quit('Data of different dimensions already created, remove current data or choose another DB name in the config file')
    else:
        __insert_or_update(pep_db, {'_id': 'sizes', 'mz_bin': mz_bin, 'rt_bin': rt_bin, 'mz_interval': mz_interval, 'rt_interval': rt_interval, 'binning': binning}, {'_id': 'sizes'})


def __get_metadata(accessions):
    if get_sconfig()['multiprocessing']['apply'] and len(accessions) > 1:
        with Pool(min(len(accessions), get_sconfig()['multiprocessing']['processes'])) as ps:
            for i, pride_results in enumerate(
                    ps.imap_unordered(partial(__get_pride_project_metadata, multiprocessing=True), accessions)):
                print(f'{i} / {len(accessions)}    ', end='\r')
    else:
        [__get_pride_project_metadata(accession, False) for accession in accessions]


def pride_project(accessions):
    if get_sconfig()['multiprocessing']['apply'] and len(accessions) > 1:
        with Pool(min(len(accessions), get_sconfig()['multiprocessing']['processes'])) as ps:
            ps.map(partial(peptide_extractor, multiprocessing=True), accessions)
    else:
        [peptide_extractor(accession, False) for accession in accessions]


def pride_data():
    projects = __get_client_dbs('ms2ai', 'projects')[0]
    pride_filter = {'maxquant_files': get_econfig()['maxquant_file'], 'source': 'pride',
                    'filetypes': {'$all': ['.zip', '.raw']}}
    accessions = projects.find(pride_filter).distinct('accession')
    pride_project(accessions)


def recreate_data(path=__get_path()):
    print(f'Fetching files')
    if path == __get_path():
        file_paths = [f'{path}{f}/{g}/' for f in next(os.walk(path))[1] for g in next(os.walk(f'{path}{f}'))[1]
                      if __recreate_file_filter(f'{path}{f}/{g}') and __recreate_db_filter(f'{path}{f}/{g}')]
    else:
        file_paths = [f'{path}{f}/' for f in os.listdir(path) if '.' not in f and __directory_filter(path, f)]

    # Check for existing peptides, and removes the paths if there's already peptides extracted
    existing_peps = __get_client_dbs('ms2ai', 'peptides')[0].distinct('identifier')
    non_existing_peps = [i for i, f in enumerate(['/'.join(f.rsplit('/')[-3:-1]) for f in file_paths]) if f not in existing_peps]
    file_paths = list(np.array(file_paths)[non_existing_peps])

    print(f'Pre-processed raw files: {len(file_paths)}')
    if get_sconfig()['multiprocessing']['apply'] and len(file_paths) > 1:
        with Pool(min(len(file_paths), get_sconfig()['multiprocessing']['processes'])) as ps:
            for i, _ in enumerate(ps.imap_unordered(partial(local_peptide_extractor, multiprocessing=True, data_path=False), file_paths)):
                print(f'{i} / {len(file_paths)}', end='\r')
    else:
        for i, accession in enumerate(file_paths):
            print(f'{i} / {len(file_paths)}', end='\r')
            local_peptide_extractor(accession, multiprocessing=False, data_path=False)


def local_data(path):
    data_paths = [f for f in path if len(set(['mq' if g.lower().endswith(('zip', 'txt')) else 'raw' for g in os.listdir(f) if g.lower().endswith(('raw', 'zip', 'txt'))])) == 2]
    [local_peptide_extractor(f, multiprocessing=False, data_path=True) for f in data_paths]
    storage_paths = [f for f in path if True in [os.path.isdir(f'{f}{g}/') for g in os.listdir(f)]]
    [recreate_data(path=f) for f in storage_paths]


def run_extractor(input, metadata):
    if not os.path.exists(__get_path()):
        quit('Path given in config.json doesn\'t exist. Please specify working path.')
    if not str(input[0]) == 'reset':
        __db_init(input[0])

    # Data resetter
    if str(input[0]) == 'reset':
        reset_data(input[1]) if len(input) > 1 else reset_data()
    # Local data extractor
    elif True in [os.path.isdir(f) or f in ['recreate', 're-create'] for f in input]:
        __prepare_formatting_software(False)
        if input[0] in ['recreate', 're-create']:
            path = [__get_path()]
        else:
            path = [f'{f}{"/" if not f.endswith("/") else ""}' for f in input]
        local_data(path)
    # Accession list
    elif str(input[0]).startswith('PXD') and not metadata:
        __prepare_formatting_software(False)
        pride_project(input)
    # Metadata fetcher
    elif str(input[0]).startswith('PXD') and metadata:
        __get_metadata(input)
    # Pride metadata
    elif input[0] == 'pride':
        __prepare_formatting_software(False)
        pride_data()
    # Print options
    else:
        quit('Run command not understood, please read the documentation and run again')
