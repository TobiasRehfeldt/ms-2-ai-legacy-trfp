import bisect

import pandas as pd

from storage.db_init import __get_client_dbs


def __import_table(file, memory_file=False):
    # Reading with csv, if that doesnt work, rewind the iterable and try a csv delimiter
    df = pd.read_csv(file, sep='\t', low_memory=False, on_bad_lines='skip')
    if len(df.columns) == 1:
        if memory_file:
            file.seek(0)
        df = pd.read_csv(file, sep=',', low_memory=False, on_bad_lines='skip')

    return df


def __get_lower_bound(list, value):
    if value == list[-1]:
        value = value - 0.000001
    return bisect.bisect(list, value) - 1


def __get_ms1_shape():
    peptides = __get_client_dbs(None, 'peptides')[0]
    ms_shape = list(peptides.find_one({'_id': 'sizes'}).values())[1:]
    return [int(ms_shape[2] / ms_shape[0] * 2 + 1), int(ms_shape[3] / ms_shape[1] * 2 + 1), 4]