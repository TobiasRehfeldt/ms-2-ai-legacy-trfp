
def __regression(cla, peptides, filtered, db):
    pep_list = [dict(list(f.items()) + list({'label': f[cla], 'name': cla}.items())) for f in peptides]
    filtered.insert_many(pep_list)
    print(f'Peptides in filtered version: {db.command("collstats", "peptides")["count"]}')