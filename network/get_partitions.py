import gzip
from collections import Counter, defaultdict
from random import shuffle

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from pyteomics import mzml
from sklearn.model_selection import train_test_split

from extractor.create_images import __image_preparameters
from extractor.imgs.create_peptxt import __get_neighbourhood
from extractor.imgs.ms2_info import __ms2_numerical_bin
from storage.db_init import __get_client_dbs
from storage.serialization import read
from extractor.common import __get_ms1_shape
from utils.config_inits import __get_path, get_image_path
from utils.config_inits import get_nconfig, get_econfig


def __get_params(filtered, ms_level, classification):
    sample = filtered.find_one({'_id': {'$ne': 'index'}})
    imp_sample = read(f"{__get_path()}images/{sample['_id']}.txt")

    ms1shape = __get_ms1_shape()
    ms1shape[-1] = len(get_nconfig()['channels'])
    ms2shape = [get_nconfig()['length_ms2'], 2]
    channel_dict = {'min': 0, 'mean': 1, 'max': 2, 'amount': 3}
    params = {'ms1shape': ms1shape,
              'ms2shape': ms2shape,
              'ms1_add': len(imp_sample[2]) if imp_sample[2] else imp_sample[2],
              'ms2_add': len(imp_sample[3]) if imp_sample[3] else imp_sample[3],
              'channels': [channel_dict[f] for f in get_nconfig()['channels']],
              'n_classes': len(filtered.distinct('label')),
              'batch_size': get_nconfig()['batch_size'],
              'classification': classification,
              'ms_level': ms_level,
              'shuffle': True,
              'name': sample['name']
              }
    return params


def __calc_test_accs(filtered):
    test_amount = (get_nconfig()['testing_percentage'] / 100) * filtered.count_documents({})
    test_accs = []
    [test_accs.append([f["_id"], f["count"]]) for f in
     list(reversed(list(filtered.aggregate([{'$sortByCount': '$accession'}])))) if
     np.sum([g[1] for g in test_accs]) < test_amount]
    test_accs = [f[0] for f in test_accs if f[0] is not None]
    test_labels = {}
    [test_labels.update({f['_id']: f['label']}) for f in filtered.find({'accession': {'$in': test_accs}})]
    test_ids = list(test_labels.keys())
    return test_accs, test_ids, test_labels


def __train_val_split(test_accs, filtered):
    train_labels, validation_labels = {}, {}

    if isinstance(filtered.find_one({'_id': {'$ne': 'index'}})['label'], int):
        label_ids = defaultdict(list)
        [label_ids[f['label']].append(f['_id']) for f in filtered.find({'_id': {'$ne': 'index'}, 'accession': {'$nin': test_accs}})]
        classification = True
        [shuffle(label_ids[f]) for f in label_ids]
        all_ids = [(label, id) for id, labels in label_ids.items() for label in labels]
        if all_ids:
            if not get_nconfig()["validation_percentage"] == 0:
                train, validation = train_test_split(all_ids, test_size=get_nconfig()["validation_percentage"] / 100)
            else:
                train, validation = all_ids, []
            train_ids = [f[0] for f in train]
            [train_labels.update({f[0]: f[1]}) for f in train]
            validation_ids = [f[0] for f in validation]
            [validation_labels.update({f[0]: f[1]}) for f in validation]
        else:
            train_ids, validation_ids = [], []

    else:
        classification = False
        id_labels = list(filtered.find({'_id': {'$ne': 'index'}, 'accession': {'$nin': test_accs}}, {'_id': 1, 'label': 1}))
        train, validation = train_test_split(id_labels, test_size=get_nconfig()["validation_percentage"] / 100)
        train_ids = [f['_id'] for f in train]
        [train_labels.update({f['_id']: f['label']}) for f in train]
        validation_ids = [f['_id'] for f in validation]
        [validation_labels.update({f['_id']: f['label']}) for f in validation]

    return classification, train_ids, train_labels, validation_ids , validation_labels


def __get_class_weights(labels):
    class_counter = Counter(labels['training'].values())
    class_weight = {}
    for f in class_counter:
        class_weight[f] = max(class_counter.values()) / class_counter[f]

    return class_weight


def __get_partitions_and_labels(filtered, test_accs, train, val):
    labels = {}
    [labels.update({f['_id']: f['label']}) for f in filtered.find({'_id': {'$ne': 'index'}})]

    test_ids = [g['_id'] for g in filtered.find({'accession': {'$in': test_accs}})]
    partition = {'train': train, 'validation': val, 'test': test_ids}
    return partition, labels


def __db_init():
    filtered, filtered_shape, db, client = __get_client_dbs(None, 'filtered', 'filtered shape')
    ms2binning, binsize = (get_econfig()['ms2_binning']['apply'], get_nconfig()['length_ms2'])
    if not ms2binning:
        filtered_shape.drop()
        filtered_shape.insert_many([f for f in filtered.find({'_id': {'$ne': 'index'}}) if f['ms2shape'][1] >= binsize])
        print(f'{filtered.count_documents({}) - filtered_shape.count_documents({})} peptides removed due to MS2 length requirements')
        filtered = filtered_shape

    return filtered


def __checkup_plot(mutable_image, pep_db, data, hist):
    # Setup plot
    fig = plt.figure(figsize=(11, 8))
    plt.set_cmap('hot')
    cmap = matplotlib.cm.get_cmap()
    cmap.set_bad('white', 1.)
    # Full scale plot
    ax1 = plt.subplot2grid((4, 5), (0, 0), colspan=5, rowspan=2)
    sns.heatmap(mutable_image[:, :, 0][::-1], ax=ax1, cbar=False, cmap=cmap, vmin=0, vmax=1)
    ax1.title.set_text(f'LC-MS Chromatogram. m/z: {pep_db["m/z"]}, retention time: {pep_db["Retention time"]}'); ax1.set_xlabel('m/z'); ax1.set_ylabel('retention time')
    ax1.tick_params(axis='both', which='both', bottom=False, left=False, labelbottom=False, labelleft=False)
    # MS1 Plot
    ax2 = plt.subplot2grid((4, 5), (2, 0), colspan=2, rowspan=2)
    sns.heatmap(data[0][:, :, 0][::-1], ax=ax2, cbar=False, cmap=cmap, vmin=0, vmax=1)
    ax2.title.set_text(f'Extracted MS1 data. \n{data[2] if data[2] else "None"}'); ax2.set_xlabel('m/z'); ax2.set_ylabel('retention time')
    ax2.tick_params(axis='both', which='both', bottom=False, left=False, labelbottom=False, labelleft=False)
    # MS2 Hist plot
    ax3 = plt.subplot2grid((4, 5), (2, 2), colspan=3, rowspan=1)
    plt.bar(range(0, len(hist)), hist)
    ax3.title.set_text('Raw MS2 data'); ax3.set_xlabel('m/z bins'); ax3.set_ylabel('intensity')
    # MS2 Plot
    ax4 = plt.subplot2grid((4, 5), (3, 2), colspan=3, rowspan=1)
    plt.bar(range(0, len(data[1][0, :])), data[1][0])
    ax4.title.set_text(f'Extracted MS2 data. \n{data[3] if data[3] else "None"}'); ax4.set_xlabel('m/z bins'); ax4.set_ylabel('intensity')
    fig.tight_layout()
    plt.show()


def __data_checkup(partition):
    # Init
    sample = [f for f in partition.values() if f][0][0]
    data = read(f'{get_image_path()}{sample}.txt')
    pep_db = __get_client_dbs(None, 'peptides')[0].find_one({'_id': sample})
    mzML = read(f'{__get_path()}{sample.split("/")[-3]}/{sample.split("/")[-2]}/mzML.txt')
    binning = get_econfig()['ms2_binning']; binning['bin_amount'] = binning['fixed_mz_length']
    hist = __ms2_numerical_bin(mzML['ms2'][str(pep_db['mzML scan'])]['m/z'], mzML['ms2'][str(pep_db['mzML scan'])]['intensity'], binning)

    # Image preparations!
    window, range_lists, resolution = __image_preparameters(sample.split('/')[-2], sample.split('/')[-3], True)
    mz_lower, mz_upper, rt_lower, rt_upper = __get_neighbourhood(range_lists, pep_db, window)
    image = read(f'{__get_path()}{sample.split("/")[-3]}/{sample.split("/")[-2]}/{resolution["x"]}x{resolution["y"]}.txt')
    mz_pad = round(resolution["x"] / 500); rt_pad = round(resolution["y"] / 150)
    padded_area = np.pad(image[rt_lower:rt_upper, mz_lower:mz_upper, :], ((rt_pad, rt_pad), (mz_pad, mz_pad), (0, 0)), constant_values=(np.nan,))
    mutable_image = np.array(image)
    mutable_image[rt_lower-rt_pad:rt_upper+rt_pad, mz_lower-mz_pad:mz_upper+mz_pad, :] = padded_area
    __checkup_plot(mutable_image, pep_db, data, hist)


def get_partitions(ms_level, checkup):
    filtered = __db_init()

    test_accessions, test_ids, test_labels = __calc_test_accs(filtered)
    classification, train_ids, train_labels, validation_ids, validation_labels = __train_val_split(test_accessions, filtered)
    partition = {'training': train_ids, 'validation': validation_ids, 'testing': test_ids}
    labels = {'training': train_labels, 'validation': validation_labels, 'testing': test_labels}
    class_weight = __get_class_weights(labels)
    params = __get_params(filtered, ms_level, classification)

    [print(f'{f} datapoints: {len(partition[f])}') for f in partition]
    if checkup:
        __data_checkup(partition)
    return params, partition, labels, test_accessions, class_weight
