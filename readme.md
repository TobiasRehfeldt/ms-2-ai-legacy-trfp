# MS2AI: Mass Spectrometry Pipeline
######Research paper release version (1.0) available [here](https://gitlab.com/tjobbertjob/ms2ai-paper-release). changelog.txt contains all changes

**Documentations are available in the *documentation.pdf* and on the GitLab Wiki!**

Pipeline to get Mass spectrometery data into Machine learning applications.
The Pipeline does everything from data acquisition, data extraction and machine learning applications, 
making it the one stop software for MS and AI implementations.

The reason data acquisition is important, is that machine learning applications needs a lot of homogenous data 
in order to be trained, which is hard to get from in-house data alone.

The pipeline is split into 4 main parts: (1) PRIDE Project metadata acquisition, 
(2) Data Extraction, (3) Metadata filtering, and (4) Machine learning application.

These parts are also separated code wise, as the potential (and average) 
runtime of the individual parts can take days or weeks to complete.

The software uses MaxQuant output files to find peptides in the MS run, 
and MaxQuant analysis of the MS data is thus a necessity for the software to be used.

## Requirements
OS: Linux, MacOS or Windows with Python 3.8

MongoDB: A working version of MongoDB with the server running

Formatting Tool: Requires a working version of Conda (MacOS or Linux) or Docker

## Installation
### Python 3.6 Packages
```
📦ms2ai
 ┣ 📂extactor
 ┣ 📂filter
 ┣ 📂network
 ┣ 📂scraper
 ┣ 📂storage
 ┣ 📂summary
 ┣ 📂tutorial
 ┣ 📂utils
 ┣ 📜config.json 
 ┣ 📜readme.md 
 ┣ 📜requirements.txt ← 
 ┗ 📜Documentation.pdf
```
*For certain packages a c-compiler is necessary (gcc) which is in build-essential on MacOS or Linux*

`pip install -r requirements.txt`

### MongoDB
######To see an explanation of all MongoDBs and their contents, see the documentation section 1.
#### Windows:
Download [MongoDB server](https://www.mongodb.com/try/download) and follow [the tutorial](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/).
Make sure to install the [Database tools](https://www.mongodb.com/try/download/tools) and add this to the path varaibles, to be able to download the current PRIDE metadata version.

#### Linux
```
sudo apt-get install mongodb
sudo service mongodb start
```
#### MacOS
###### Requires Homebrew installation. Skip first line if Homebrew is already installed
```badh
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)
brew install node
brew install mongo
sudo mkdir -p /data/db
sudo chmod -arwx /data/db
```

### Get PRIDE metadata
The in-built method will pull the current version of the collection, and import it into the 'ms2ai' database:
```
python api.py -db
```

### Formatting Software
#### Conda
```bash
# Linux
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O Miniconda3.sh

# MacOS
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh -O Miniconda3.sh

sh Miniconda3.sh
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge
conda install -y -c bioconda thermorawfileparser
conda install conda
conda update --force conda
```

### Docker

Install Docker ([Windows](https://docs.docker.com/desktop/windows/install/), [Mac](https://docs.docker.com/desktop/mac/install/), [Linux](https://docs.docker.com/engine/install/ubuntu/)
) and run this code in your cmd or terminal. Tutorials:

```bash
docker pull veitveit/trp
pip install docker
```

## Data-path directory structure
```
📦/path/to/data
 ┣ 📂images
 ┃   ┗📂database-name
 ┃      ┗📂PXD projects
 ┃         ┗📂raw files
 ┃            ┗📜peptide-representation-ID.txt
 ┣ 📂metadata
 ┃   ┣📜best-model.h5
 ┃   ┣📜network-model.png
 ┃   ┣📜historic-plot.png
 ┃   ┗📂Tensorboard/WandB log
 ┣ 📂PXD projects
 ┃   ┗📂raw files
 ┃      ┣ 📜file.raw
 ┃      ┣ 📜file.mzML.gz
 ┃      ┣ 📜mzML.txt
 ┃      ┣ 📜MaxQuant.txt
 ┗      ┗ 📜XxY.txt
```
In the configuration file there is a path parameter which must be set to an already created folder. 
This can be an absolute or relative path on the local machine, but must exist pre running the software.

Within this directory will be created directories for all projects extracted, aswell as a metadata and an image directory.

The image directory will be the palce where all peptide .txt files are saved, and is automatically pulled 
into the machine learning application.

The metadata holds the information from the neural networks: the history loss plots, the model overviews, 
and the model saved model and weights. This is to be used if you want to check how a neural networks is performing
and share the results with others. This is also where models should be placed if you want to test others models
using our software.


## Usage:
```
📦ms2ai
 ┣ 📂extactor
 ┣ 📂filter
 ┣ 📂network
 ┣ 📂scraper
 ┣ 📂storage
 ┣ 📂summary
 ┣ 📂tutorial ←
 ┣  ┣📜tutorial.py ←
 ┣  ┗📜tutorial.h5 
 ┣ 📂utils
 ┣ 📜config.json 
 ┣ 📜readme.md 
 ┣ 📜requirements.txt 
 ┗ 📜Documentation.pdf ←
```
MS2AI comes with an indepth documentation, and a functional tutorial run.
The documentation illustrates all the possible use cases of the software,
along with in-depth examples to show the utility.