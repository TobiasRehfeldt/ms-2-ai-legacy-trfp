import os

from simplejson import loads


def get_config():
    file_location = __file__.replace("\\", "/")
    return loads(open(f'{file_location.rsplit("/", 2)[0]}/config.json').read())


def get_sconfig():
    return get_config()['storage']


def get_econfig():
    return get_config()['extractor']


def get_fconfig():
    return get_config()['filter']


def get_nconfig():
    return get_config()['network']


def __get_path():
    base = get_sconfig()['path']
    return base if base.endswith('/') else f'{base}/'


def get_metadata_path():
    return __get_path() + 'metadata/'


def get_image_path():
    return __get_path() + 'images/'


def __remove_used(file, force_remove=False):
    if (get_config()['extractor']['remove_used_files'] or force_remove) and os.path.exists(file):
        return os.remove(file)
