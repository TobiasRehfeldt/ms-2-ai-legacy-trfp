import shutil
from glob import glob

from storage.db_init import __get_client_dbs
from utils.common import __validated_input
from utils.config_inits import get_sconfig, get_image_path, __get_path


def reset_data(db_name=None, force_reset=False):
    if db_name == 'all':
        from pymongo import MongoClient
        client = MongoClient()
        db_names = [f['name'] for f in client.list_databases() if 'peptides' in client[f['name']].list_collection_names()]
    elif db_name is None:
        db_names = [get_sconfig()['db_name']]
    else:
        db_names = [db_name]
    peps = [__get_client_dbs(f, 'peptides')[0] for f in db_names]

    if not force_reset:
        for db in peps:
            print(f'\nDatabase: {db.full_name.split(".")[0]}')
            print('{:,}'.format(len(glob(f'{get_image_path()}{db.full_name.split(".")[0]}/*'))), 'projects')
            print('{:,}'.format(len(glob(f'{get_image_path()}{db.full_name.split(".")[0]}/*/*'))), 'raw files')
            print('{:,}'.format(db.count_documents({})), 'images')

    if force_reset or __validated_input('Sure you want to reset peptide data?', ['y', 'n'], equals_to='y', default_option='y'):
        for db in db_names:
            peptides, filtered = __get_client_dbs(db, 'peptides', 'filtered')[:2]

            shutil.rmtree(f'{__get_path()}images/{db}', ignore_errors=True)  # Once to remove files
            shutil.rmtree(f'{__get_path()}images/{db}', ignore_errors=True)  # Twice to remove directories. This is needed... for some reason
            peptides.drop()
            filtered.drop()