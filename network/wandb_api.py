import os

import numpy as np

from storage.db_init import __get_client_dbs
from storage.serialization import read
from utils.config_inits import get_nconfig, get_image_path

wandb_init = get_nconfig()['WandB']


def __wandb_name(ms_level, example):
    sizes = __get_client_dbs(None, 'peptides')[0].find_one({'_id': 'sizes'})
    data = {
        'ms1': [str(f) for f in tuple(list(np.array(example[0]).shape[:2]) + get_nconfig()['channels'])],
        'ms1+': [str(f) for f in np.array(example[2]).shape],
        'ms2': [str(f) for f in np.array([sizes['binning'][1] if sizes['binning'][0] else get_nconfig()['length_ms2'], 2])],
        'ms2+': [str(f) for f in np.array(example[3]).shape]
    }
    name_dict = {}
    for f in ms_level:
        ms = f.split('+')[0]
        for g in f.split('+'):
            name_dict[f'{ms}{"+" if not g else ""}'] = data[f'{ms}{"+" if not g else ""}']

    return ' | '.join([f'{f}: {", ".join(name_dict[f])}' for f in name_dict])


def __wandb_init(ms_level, example):
    import wandb
    project_name = __wandb_name(ms_level, read(f'{get_image_path()}{example}.txt'))
    if wandb_init['apply']:
        team_name = wandb_init['team_name'] if wandb_init['team_name'] else wandb.api.default_entity
        wandb.init(project=wandb_init['project_name'], name=project_name, entity=team_name)


def __wandb_savemodel(model):
    import wandb

    if wandb_init['apply']:
        model.save(os.path.join(wandb.run.dir, "model.h5"))


def _wandb_callbacks(callbacks, datagens):
    from wandb.keras import WandbCallback

    if wandb_init['apply']:
        # wandb.init(project=wandb_init['project_name'])
        callbacks.append(WandbCallback(labels=__get_client_dbs(None, 'filtered')[0].find_one({'_id': 'index'})['classes'],
                                       training_data=datagens[0], validation_data=datagens[1], predictions=16))
    return callbacks


def __wandb_confusion(labels, predictions, label_names):
    import wandb
    if wandb_init['apply']:
        wandb.log({"my_conf_mat_id": wandb.plot.confusion_matrix(preds=predictions, y_true=labels, class_names=list(label_names.values()))})


