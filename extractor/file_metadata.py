import zipfile

import numpy as np
import pandas as pd
from pandas import DataFrame

from extractor.common import __import_table
from extractor.imgs.ms2_info import __ms2_scan_name
from extractor.xml_parser import __xml_parser
from pride_api.keys import __pride_keys
from storage.compressions import __get_compressed_file, __ext_from_zip
from storage.db_init import __insert_or_update, __get_client_dbs
from utils.config_inits import get_econfig
from utils.log import ms2ai_log, __internal_print


def __column_renamer(df):
    df = df.rename(columns={f: g for f in df for g in ['Raw file', 'Retention time'] if f.lower() == g.lower()})
    df['Raw file'] = df['Raw file'].apply(str)
    return df


def __mq_file_handler(mq_files):
    if mq_files:
        # Import and concatenates all the mq files into one dataframe, Drops all na's in the Sequence and ms/ms scan number columns
        mq_df = pd.concat([__import_table(file=f, memory_file=True) for f in mq_files])
        mq_df.replace(' ', np.nan, inplace=True)
        mq_df.dropna(subset=['Sequence', __ms2_scan_name(mq_df)], inplace=True)
        # replaces any misnamed 'Raw file' column with the proper name, and retrieve all unique raw file names.
        mq_df = __column_renamer(mq_df)
        rawfiles = list(set(mq_df['Raw file']))
        return mq_df, rawfiles

    else:
        # If no MaxQuant file exists, return an empty DataFrame and Rawfile list for downstream error issues
        return DataFrame(), []


def __summary_keys():
    return ['Raw file', 'Fraction', 'Enzyme', 'Enzyme mode', 'Enzyme first search', 'Use enzyme first search',
            'Variable modifications', 'Multi modifications', 'Variable modifications first search', 'Use variable modifications first search',
            'Requantify', 'Multiplicity', 'Max. missed cleavages', 'Max. labeled AAs', 'Labels0', 'Labels1', 'Labels2',
            'LC-MS run type', 'Time-dependent recalibration', 'MS', 'MS/MS', 'MS/MS Submitted', 'MS/MS Identified',
            'Peptide Sequences Identified', 'Peaks', 'Peaks Sequenced', 'Peaks Sequenced [%]', 'Peaks Repeatedly Sequenced',
            'Peaks Repeatedly Sequenced [%]', 'Isotope Patterns', 'Isotope Patterns Sequenced', 'Isotope Patterns Sequenced [%]',
            'Isotope Patterns Repeatedly Sequenced', 'Isotope Patterns Repeatedly Sequenced [%]', 'Multiplets', 'Multiplets Sequenced',
            'Multiplets Sequenced [%]', 'Multiplets Identified', 'Multiplets Identified [%]', 'Recalibrated', 'MBRFDR',
            'Label free norm param', 'Av. Absolute Mass Deviation [ppm]', 'Mass Standard Deviation [ppm]',
            'Av. Absolute Mass Deviation [mDa]', 'Mass Standard Deviation [mDa]']


def __summary_handler(summary_files, rawfiles, accnr):
    if bool(summary_files):
        # Import and concatenates all the summary files into one dataframe, and replaces any misnamed 'Raw file' column with the proper name, and retrieve all unique raw file names.
        summary = pd.concat([__import_table(file=f, memory_file=True) for f in summary_files])
        summary = __column_renamer(summary)
        # get a dictionary for each rawfile entry in the summary df, replace entries with '.' and insert informations
        summary_dicts = [summary.loc[summary['Raw file'] == f, ].iloc[0].to_dict() for f in rawfiles if f in list(summary['Raw file'])]
        summary_dicts = [{k.replace('.', '_'): v if not isinstance(f[k], np.int64) else int(v) for k, v in f.items() if k in __summary_keys()} for f in summary_dicts]
        summary_dicts = [{**{'accession': accnr, 'Raw file': str(f['Raw file']), 'identifier': f'{accnr}/{f["Raw file"]}', 'summary_exists': True}, **f} for f in summary_dicts]
        # insert the dictionary into the 'files' db
        [__insert_or_update(__get_client_dbs('ms2ai', 'files')[0], sum_file, ['identifier']) for sum_file in summary_dicts]

    else:
        # If no summary file exists, report that no summary file exists
        summary_missing = [{'accession': accnr, 'Raw file': f, 'identifier': f'{accnr}/{f}', 'summary_exists': False} for f in rawfiles]
        [__insert_or_update(__get_client_dbs('ms2ai', 'files')[0], sum_file, ['identifier']) for sum_file in summary_missing]


def __project_handler(accnr, rawfiles):
    file_info, projects = __get_client_dbs('ms2ai', 'files', 'projects')[:2]
    if bool(projects.find_one({'accession': accnr})):
        project_info = {k: v for k, v in projects.find_one({'accession': accnr}).items() if k in __pride_keys()}
        [__insert_or_update(file_info, {**{'accession': accnr, 'Raw file': f, 'identifier': f'{accnr}/{f}', 'project_exists': True}, **project_info}, ['identifier']) for f in rawfiles]
    else:
        [__insert_or_update(file_info, {'accession': accnr, 'Raw file': f, 'identifier': f'{accnr}/{f}', 'project_exists': False}, ['identifier']) for f in rawfiles]


def __rawfile_metadata(zip_file, accnr, multiprocessing):
    __internal_print('Getting zipfile', multiprocessing)
    # Get MQ information from a ZipFile element
    if isinstance(zip_file, zipfile.ZipFile):
        mq_file, summary_file, xml_files = __ext_from_zip([get_econfig()['maxquant_file'], 'summary.txt', '.xml'], zip_file=zip_file)
    # Get MQ information from a zipfile link
    else:
        zipfile_url = zip_file.translate(str.maketrans({" ": r"%20"}))
        mq_file, summary_file, xml_files = __ext_from_zip([get_econfig()['maxquant_file'], 'summary.txt', '.xml'], url=zipfile_url)

    __internal_print('Handling metadata', multiprocessing)
    # Handle the MaxQuant identification file
    mq_df, rawfiles = __mq_file_handler(mq_file)
    # Handle summary file
    __summary_handler(summary_file, rawfiles, accnr)
    # Handle project information
    __project_handler(accnr, rawfiles)
    # Handle XML files
    __xml_parser(xml_files, accnr, rawfiles)
    return rawfiles, mq_df