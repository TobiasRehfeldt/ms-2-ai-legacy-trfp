import os

import pandas as pd

from extractor.file_metadata import __rawfile_metadata
from extractor.mzml_metadata import __mzML_info
from extractor.xml_parser import __xml_parser, __loose_xmls
from pride_api.get_urls import __get_zip_files, __get_raw_files, __get_files_api
from storage.serialization import __create_file
from utils.config_inits import __get_path, get_econfig
from utils.log import ms2ai_log


def __get_pride_project_metadata(accnr, multiprocessing):
    try:
        # Find all zip files, rawfiles
        files_api = __get_files_api(accnr)
        zipfiles, rawfiles = __get_zip_files(files_api), __get_raw_files(files_api)
        __xml_parser(__loose_xmls(accnr), accnr, [])

        for zips in zipfiles:
            try:
                (zip_rawfiles, df) = __rawfile_metadata(zips, accnr, multiprocessing)
                # Finding which zip files matches the naming schemes
                rawfile_names = [f.split('/')[-1].split('.')[0] for f in rawfiles if f.split('/')[-1].split('.')[0] in zip_rawfiles]
                if not bool(rawfile_names):
                    continue

                for filename in rawfile_names:
                    try:
                        filepath = f'{__get_path()}{accnr}/{filename}/'
                        __create_file(filepath)
                        raw_df = df.loc[df['Raw file'] == filename, ]
                        if not os.path.exists(f'{filepath}{get_econfig()["maxquant_file"]}'):
                            pd.DataFrame.to_csv(raw_df, f'{filepath}{get_econfig()["maxquant_file"]}', index=False)

                        if os.path.exists(f'{filepath}mzML.txt'):
                            __mzML_info(filepath, filename, accnr, multiprocessing)
                    except Exception as e:
                        # traceback.print_exc()
                        ms2ai_log(f'Raw: {accnr}/{filename} ({zips.split("/")[-1]}), {e.__class__.__name__}: {e}', 'info')
            except Exception as e:
                # traceback.print_exc()
                ms2ai_log(f'Zip: {"/".join(zips.split("/")[-2:])}, {e.__class__.__name__}: {e}', 'info')
    except Exception as e:
        # traceback.print_exc()
        ms2ai_log(f'Project: {accnr}, {e.__class__.__name__}: {e}', 'info')