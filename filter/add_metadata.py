from itertools import chain

import xmltodict
import yaml
from simplejson import loads
import pandas as pd
from extractor.common import __import_table
from extractor.init_local import __new_accnr
from extractor.xml_parser import __xml_parser, __flatten_dict, __xml_keys
from storage.db_init import __get_client_dbs, __insert_or_update


def __yaml_parser(path):
    yaml_file = yaml.safe_load(open(path).read())
    data = list(chain(*[yaml_file[f] for f in yaml_file]))
    yaml_data = {}
    [yaml_data.update({f['name']: [f['value']]}) for f in data]

    return pd.DataFrame.from_dict(yaml_data)


def __get_project(data):
    projectname_column = [f for f in data.columns if f.lower() in ['accession', 'accession2', 'project', 'projects']]
    if not 'accession' in data.columns:
        projectname_column = __new_accnr(default_option='use')
        data['accession'] = projectname_column
    else:
        data = data.rename(columns={projectname_column[0]: 'accession'})
    return data


def __get_files(data):
    filename_column = [f for f in data.columns if f.lower() in ['raw file', 'filename', 'file name']]
    if not filename_column:
        quit('No column to cross-reference with file names.')
    else:
        data = data.rename(columns={filename_column[0]: 'Raw file'})
        # data['Raw file'] = data['Raw file'].apply(str)
        data['Raw file'] = data['Raw file'].str.rsplit('.', 1, expand=True)[0]
    return data


def __import_data(path):
    if path.endswith('json'):
        data = loads(open(path).read())
        data = pd.DataFrame.from_dict(data)

    elif path.endswith('yaml'):
        data = __yaml_parser(path)

    elif path.endswith('xml'):
        xml_dict = dict(xmltodict.parse('\n'.join(open(path).readlines()[1:])))
        if len(xml_dict) == 1:
            xml_dict = xml_dict[list(xml_dict.keys())[0]]
        xml_dict = __flatten_dict(xml_dict)
        data = [{'Raw file': g.replace('\\', '/').split('/')[-1].split('.')[0]} for g in xml_dict['filePaths']]
        base_info = {f: xml_dict[f] for f in xml_dict if f in __xml_keys()}
        [f.update(base_info) for f in data]
        data = pd.DataFrame.from_dict(data)

    else:
        data = __import_table(path)
        if path.endswith('parameters.txt'):
            data = data.T
            data.columns = data.iloc[0]
            data = data[1:]

    return data


def __add_metadata(path, file):
    print(f'Appending data from {path} to the projects database')
    data = __import_data(path)
    data = __get_project(data)

    if file == 'files':
        data = __get_files(data)
        data = pd.DataFrame.to_dict(data, 'records')
        files = __get_client_dbs('ms2ai', 'files')[0]
        [f.update({'identifier': f'{f["accession"]}/{f["Raw file"]}'}) for f in data]
        [__insert_or_update(files, f, ['accession']) for f in data]

    if file == 'projects':
        data = pd.DataFrame.to_dict(data, 'records')
        projects = __get_client_dbs('ms2ai', 'projects')[0]
        __insert_or_update(projects, data[0], ['accession'])