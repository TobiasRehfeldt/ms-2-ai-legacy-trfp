from keras.applications.vgg19 import VGG19
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, TensorBoard
from tensorflow.keras import optimizers
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Activation, BatchNormalization, Dense, Input, Conv2D, MaxPooling2D, Concatenate, GlobalAveragePooling2D, Flatten, Lambda, LocallyConnected2D
from tensorflow.keras import backend as K
from network.metrics import r2_metric
from network.wandb_api import _wandb_callbacks
from utils.config_inits import get_nconfig, get_metadata_path
from tensorflow.keras.optimizers.schedules import ExponentialDecay


class ms2aiModel:
    def __init__(self, params):
        self.params = params

    ############# First layers #############
    def phase1_ms1(self):
        input_l = Input(shape=tuple(self.params['ms1shape']), name='ms1_input')
        # x = Flatten(name='ms1_flattening')(input_l)

        # VGG Style
        # x = Conv2D(16, kernel_size=(3, 3), dilation_rate=(2, 2), activation='relu')(input_l)
        # x = Conv2D(16, kernel_size=(3, 3), dilation_rate=(2, 2), activation='relu')(x)
        # x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2))(x)
        # x = Conv2D(16, kernel_size=(3, 3), dilation_rate=(2, 2), activation='relu')(x)
        # x = Conv2D(16, kernel_size=(3, 3), dilation_rate=(2, 2), activation='relu')(x)
        # x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2))(x)

        # Google Inception Style
        # kernel = (5, 5)
        # x1 = Conv2D(64, kernel_size=kernel, dilation_rate=(2, 2), activation='relu', name=f'ms1_conv_{str(kernel[0])}x{str(kernel[1])}')(input_l)
        # # x1 = MaxPooling2D(pool_size=(2, 2), name=f'ms1_{str(kernel[0])}x{str(kernel[1])}_maxpool')(x1)
        # x1 = GlobalAveragePooling2D(name=f'ms1_{str(kernel[0])}x{str(kernel[1])}_GAP')(x1)
        # kernel = (7, 7)
        # x2 = Conv2D(64, kernel_size=kernel, dilation_rate=(2, 2), activation='relu', name=f'ms1_conv_{str(kernel[0])}x{str(kernel[1])}')(input_l)
        # # x2 = MaxPooling2D(pool_size=(2, 2), name=f'ms1_{str(kernel[0])}x{str(kernel[1])}_maxpool')(x2)
        # x2 = GlobalAveragePooling2D(name=f'ms1_{str(kernel[0])}x{str(kernel[1])}_GAP')(x2)
        # x = Concatenate()([x1, x2])

        # Locally Connected Layers
        # kernel = (2, 2)
        # x = LocallyConnected2D(16, kernel_size=kernel, activation='relu', name=f'ms1_{str(kernel[0])}x{str(kernel[1])}_LCD')(input_l)
        # x = Flatten(name=f'ms1_{str(kernel[0])}x{str(kernel[1])}_flatten')(x)
        #
        # kernel = (3, 3)
        # x1 = LocallyConnected2D(16, kernel_size=kernel, activation='relu', name=f'ms1_{str(kernel[0])}x{str(kernel[1])}_LCD')(input_l)
        # x1 = Flatten(name=f'ms1_{str(kernel[0])}x{str(kernel[1])}_flatten')(x1)
        #
        # kernel = (5, 5)
        # x2 = LocallyConnected2D(16, kernel_size=kernel, activation='relu', name=f'ms1_{str(kernel[0])}x{str(kernel[1])}_LCD')(input_l)
        # x2 = Flatten(name=f'ms1_{str(kernel[0])}x{str(kernel[1])}_flatten')(x2)
        #
        # x = Concatenate()([x, x1, x2])

        x = Flatten(name='ms1_flattening')(input_l)
        x = Dense(512, activation='relu')(x)

        return input_l, x

    def phase1_ms2(self):
        # input_l = Input(shape=(self.ms2size[0], 1) if get_config()['extractor']['ms2_binning']['apply'] else (None, 2), name='ms2_lstm_input')  # (Seq_len, 2) 2:[m/z, int]
        # x = LSTM(128)(input_l)
        input_l = Input(shape=tuple(self.params['ms2shape']), name='ms2_input')
        x = Flatten(name='ms2_flatten')(input_l)
        x = Dense(512, activation='relu', name='ms2_dense_1')(x)
        x = Dense(256, activation='relu', name='ms2_dense_2')(x)
        x = Dense(256, activation='relu', name='ms2_dense_3')(x)

        return input_l, x

    def phase1_add_ms1_info(self):
        input_l = Input(shape=(self.params['ms1_add']), name='ms1+_input')
        x = Dense(256, activation='relu', name='add_ms1_dense_1')(input_l)

        return input_l, x

    def phase1_add_ms2_info(self):
        input_l = Input(shape=(self.params['ms2_add']), name='ms2+_input')
        x = Dense(256, activation='relu', name='add_ms2_dense_1')(input_l)

        return input_l, x

    ############# Second layers #############
    def phase2_comb(self):
        network_output = Dense(256, activation='relu', name='comb_input')(self.p1_output_layer)
        network_output = Dense(256, activation='relu', name='comb_output')(network_output)
        return network_output

    ############# Output layers #############
    def phase3_comb(self):
        if self.params['classification']:
            return Dense(self.params['n_classes'], activation='softmax', name='output_softmax')
        else:
            return Dense(self.params['n_classes'], activation='linear', name='output_linear')

    # Combining the Network
    def get_network(self):
        data = {
            'ms1': self.phase1_ms1(),
            'ms1+': self.phase1_add_ms1_info(),
            'ms2': self.phase1_ms2(),
            'ms2+': self.phase1_add_ms2_info()
        }

        model_dict = {}
        for f in self.params['ms_level']:
            ms = f.split('+')[0]
            for g in f.split('+'):
                model_dict[f'{ms}{"+" if not g else ""}'] = data[f'{ms}{"+" if not g else ""}']

        p1_input_layer = [f[0] for f in list(model_dict.values())]
        p1_outputs = [f[1] for f in list(model_dict.values())]
        self.p1_output_layer = Concatenate()(p1_outputs) if len(p1_outputs) > 1 else p1_outputs[0]
        p2_output_layer = self.phase2_comb()
        p3_output_layer = self.phase3_comb()(p2_output_layer)
        model = Model(inputs=p1_input_layer, outputs=p3_output_layer)

        LR = ExponentialDecay(
                get_nconfig()['learning_rate'],
                decay_steps=2000,
                decay_rate=1)
        compiler_dict = {True: {'optimizer': optimizers.Adam, 'loss': 'categorical_crossentropy', 'metrics': ['accuracy']},
                         False: {'optimizer': optimizers.RMSprop, 'loss': 'mse', 'metrics': [r2_metric]}}
        model.compile(loss=compiler_dict[self.params['classification']]['loss'],
                      metrics=compiler_dict[self.params['classification']]['metrics'],
                      optimizer=compiler_dict[self.params['classification']]['optimizer'](learning_rate=LR))
        # if self.params['classification']:
        #     model.compile(loss='categorical_crossentropy', metrics=['accuracy'], optimizer=opt)
        # else:
        #     model.compile(loss='mse', metrics=[r2_metric], optimizer=opt)

        return model

    def get_callbacks(self, datagens):
        checkpoint = ModelCheckpoint(f'{get_metadata_path()}{self.params["name"]}-{"_".join(self.params["ms_level"])}.h5',
                                     monitor='val_accuracy' if self.params['classification'] else 'val_r2', save_best_only=True)
        early_stopping = EarlyStopping(monitor='val_accuracy', patience=get_nconfig()['early_stopping'])
        tensorboard_callback = TensorBoard(log_dir=get_metadata_path() + 'logs/', update_freq='batch')

        callbacks = [checkpoint, early_stopping, tensorboard_callback]
        callbacks = _wandb_callbacks(callbacks, datagens)

        return callbacks


def __get_model(model, params):
    if model == 'ms2ai':
        return ms2aiModel(params).get_network()

    elif model == 'prosit':
        from dlomix.models.prosit import PrositRetentionTimePredictor
        # return PrositRetentionTimePredictor().call()

    elif model == 'deeplc':
        from dlomix.models.deepLC import DeepLCRetentionTimePredictor

    elif model == 'dlomix':
        from dlomix.models.base import RetentionTimePredictor