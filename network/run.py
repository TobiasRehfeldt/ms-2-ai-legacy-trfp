import os
from tensorflow.python.keras.utils.vis_utils import plot_model
from network.model import ms2aiModel
from network.wandb_api import __wandb_init, __wandb_savemodel
from network.sweep_api import __wandb_sweep
from utils.config_inits import get_metadata_path, get_nconfig, get_sconfig
from storage.serialization import __create_file

if get_sconfig()['log_level'].upper() not in ['NOTSET', 'DEBUG']:
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # Mutes tensorflow prints
    os.environ["WANDB_SILENT"] = "true"

import random
import numpy as np
import matplotlib.pyplot as plt
from network.datagenerator import DataGenerator
from network.get_partitions import get_partitions
from network.network_test import __test_model
from storage.db_init import __get_client_dbs


def __set_seeds():
    setseed = get_nconfig()['replication_seed']

    if setseed['use_seed']:
        random.seed(setseed['seed'])
        np.random.seed(setseed['seed'])
        os.environ['PYTHONHASHSEED'] = str(setseed['seed'])


def __set_init():
    if not __get_client_dbs(None, 'filtered')[0].find_one({'_id': {'$ne': 'index'}}):
        quit(f'Filter peptides before running network')
    __create_file(get_metadata_path())
    __set_seeds()


def __network_model(params, datagens):
    model_network = ms2aiModel(params)
    model = model_network.get_network()
    callbacks_list = model_network.get_callbacks(datagens)
    try:
        print(model.summary())
        if os.path.exists(f'{get_metadata_path()}model_{"_".join(params["ms_level"])}.png'):
            os.remove(f'{get_metadata_path()}model_{"_".join(params["ms_level"])}.png')
        plot_model(model, to_file=f'{get_metadata_path()}model_{"_".join(params["ms_level"])}.png', show_shapes=True)
    except:
        print('Graphviz not installed (install: https://graphviz.org/), cannot save model image')

    __wandb_savemodel(model)

    return model, callbacks_list


def __performance_plot(history, metric, imageclass, n_classes, ms_level):
    plt.plot(history.history[f'{metric}'])
    plt.plot(history.history[f'val_{metric}'])
    plt.title(f'Plot of validation {metric} over time, on {n_classes} class Charge')
    plt.ylabel(f'{metric}')
    plt.xlabel('Epoch')
    plt.legend(['Training', 'Validation'], loc='upper left')
    plt.savefig(f'{get_metadata_path()}{imageclass.replace("/", "")}_{"_".join(ms_level)}.png')


def __get_performance_plot(history, classification, name, n_classes, ms_level):
    if classification:
        __performance_plot(history, 'accuracy', name, n_classes, ms_level)
    else:
        __performance_plot(history, 'r2', name, n_classes, ms_level)


def run_network(ms_level, test, test_model, checkup):
    __set_init()
    params, partition, labels, test_accs, class_weight = get_partitions(ms_level, checkup)

    if test or test_model:
        return __test_model(partition, labels, params, test_model)

    training_generator = DataGenerator(partition['training'], labels['training'], params)
    validation_generator = DataGenerator(partition['validation'], labels['validation'], params)

    __wandb_init(params['ms_level'], [f for f in partition.values() if f][0][0])
    model, callbacklist = __network_model(params, [training_generator, validation_generator])
    if partition['training']:
        history = model.fit(x=training_generator, validation_data=validation_generator, epochs=get_nconfig()['epochs'],
                            callbacks=callbacklist, class_weight=class_weight)
    else:
        quit('No training data available!')

    __get_performance_plot(history, params['classification'], params['name'], params['n_classes'], ms_level)
    return __test_model(partition, labels, params, test_model, wandb_confusion=True)