from itertools import chain
from multiprocessing.pool import Pool

from pride_api.common import __get_json
from pride_api.get_urls import __base_metadata_page


def __metadata_list_extender(entry):
    inter_dict = {}
    for key in entry:
        if isinstance(entry[key], list) and entry[key] and isinstance(entry[key][0], dict):
            for lower_keys in entry[key][0]:
                if '@' not in lower_keys and 'cv' not in lower_keys and '_' not in key:
                    if lower_keys == 'name':
                        inter_dict[key] = entry[key][0][lower_keys]
                    else:
                        inter_dict[f'{key} {lower_keys}'] = entry[key][0][lower_keys]
        elif isinstance(entry[key], dict):
            pass
        else:
            inter_dict[key] = entry[key]
    [inter_dict.update({f: None}) for f in inter_dict if inter_dict[f] == "" or not inter_dict[f]]
    return inter_dict


def __metadata_base():
    info = __get_json('https://www.ebi.ac.uk/pride/ws/archive/v2/projects?page=1&sortDirection=DESC&sortConditions=submissionDate')['page']
    page_amount = info['totalPages']
    project_amount = info['totalElements']

    metadata = []
    with Pool(16) as ps:
        for i, pages in enumerate(ps.imap_unordered(__base_metadata_page, list(range(0, page_amount)))):
            if i * 100 % 250 == 0:
                print(f'Pulling base metadata: {i*100}/{project_amount}', end='\r')
            metadata.append(pages)

    metadata2 = []
    for entries in list(chain(*metadata)):
        metadata2.append(__metadata_list_extender(entries))

    [f.update({'maxquant': 'maxquant' in str(f).lower()}) for f in metadata2]
    print(f'Base metadata pulled!                             ')
    return metadata2


