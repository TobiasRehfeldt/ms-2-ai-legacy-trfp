from time import sleep

import requests


def __get_json(url):
    for attempts in range(50):
        try:
            return requests.get(url).json()
        except Exception as e:
            if requests.get(url).status_code == 204:  # This error code is only presented if the site doesnt exist
                return {}
            sleep(2)
    return {}


def __get_link(file):
    index = 0 if file['publicFileLocations'][0]['name'] == 'FTP Protocol' else 1
    return file['publicFileLocations'][index]['value']


def __get_size(file):
    return file['fileSizeBytes']
