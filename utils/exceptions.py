

class mzMLScanError(Exception):
    def __init__(self):
        super().__init__('mzML scans do not correspond with MaxQuant information')
    pass


class condaInstallationError(Exception):
    def __init__(self):
        super().__init__('Attempt to install ThermoRawFileParser via Conda failed. \nMake sure Python version is 3.8 and to "pip/conda install conda"')
    pass


class dockerInstallationError(Exception):
    def __init__(self):
        super().__init__('Attempt to install ThermoRawFileParser via Docker failed. \nMake sure Python version is 3.8 and to "pip install docker"')
    pass