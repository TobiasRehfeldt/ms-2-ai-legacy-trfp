import os

from storage.db_init import __get_client_dbs
from utils.config_inits import get_sconfig, get_econfig


def __skip_existing_pepfiles(pep_db_name):
    return os.path.exists(f"/data/tobias/images/{pep_db_name}")


def __skip_outofbounds(subimage_interval, image):
    return subimage_interval[0] < 0 or subimage_interval[1] > image.shape[0] or \
           subimage_interval[2] < 0 or subimage_interval[3] > image.shape[1]


def __custom_filter(pep):
    return pep['mzML m/z difference'] > 2


def peptide_filters(pep, bounds, image, pep_db_name, ms_extraction):
    if __skip_existing_pepfiles(pep_db_name):
        return True

    if __skip_outofbounds(bounds, image) and 'ms1' in ms_extraction:
        return True

    if __custom_filter(pep):
        return True

    return False


def __recreate_file_filter(filter_path):
    return all([item in next(os.walk(filter_path))[2] for item in ['mzML.txt', get_econfig()['maxquant_file']]])


def __recreate_db_filter(path):
    projects_db, files_db = __get_client_dbs('ms2ai', 'projects', 'files')[:2]
    return bool(projects_db.find_one({'accession': path.split('/')[-2]})) and \
           bool(files_db.find_one({'identifier': '/'.join(path.split('/')[-2:])}))


def __directory_filter(path, directory):
    contains_exts = [g.split('.')[-1] for g in os.listdir(f'{path}{directory}/') if g.endswith(('raw', 'zip', 'txt'))]
    return len(set(contains_exts)) > 1

