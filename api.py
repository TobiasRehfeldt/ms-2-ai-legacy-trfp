import argparse
import logging
import sys


def setup_args():
    parser = argparse.ArgumentParser(description='MS2AI Pipeline options')
    # Scraper
    parser.add_argument('-s', '--scraper', help='Acquires the metadata from all public PRIDE projects', action='store_true')
    # Filter
    parser.add_argument('-f', '--filter', type=str, help='Filter the peptide metadata based on a given class', metavar="")
    parser.add_argument('-b', '--binary_filter', type=str, help='Added to the filter to create a binary filtering', metavar="")
    parser.add_argument('-cc', '--class_count', type=int, help='Added to the filter to create n different filter classes based on abundance', metavar="")
    parser.add_argument('-cl', '--class_list', type=str, nargs='+', help='Added to the filter to create n classes based on a list passed by the user', metavar="")
    parser.add_argument('-q', '--query', type=str, nargs='+', help='A query added to the filter, to give the user more control over the filtered peptides', metavar="")
    # Filter - Additional Info
    parser.add_argument('-a1', '--add_ms1_info', type=str, nargs='+', help='Add information to the MS1 part of the peptide data representation', metavar="")
    parser.add_argument('-a2', '--add_ms2_info', type=str, nargs='+', help='Add information to the MS2 part of the peptide data representation', metavar="")
    # Addition of metadata files
    parser.add_argument('-amp', '--add_metadata_projects', type=str, help='Add metadata to projects db based on path and format', metavar="")
    parser.add_argument('-amf', '--add_metadata_files', type=str, help='Add metadata to files db based on path and format', metavar="")
    # Extractor
    parser.add_argument('-e', '--extract', type=str, nargs='+', help='Formats, handles and extracts peptide information from raw files based on the conjoined MaxQuant files', metavar="")
    parser.add_argument('-m', '--metadata', help='Get only the metadata for the following projects', action='store_true')
    # ML implementation
    parser.add_argument('-n', '--network', type=str, nargs='+', help='Trains or tests a neural network based on the filtered metadata', metavar="")
    parser.add_argument('-t', '--test', help='Runs a test of the store network based on the filtered metadata', action='store_true')
    parser.add_argument('-tm', '--test_model', type=str, help='Run neural network tests on a specific model in the metadata folder', metavar="")
    parser.add_argument('-c', '--checkup', help='Checks a file to see that inputs are correctly setup', action='store_true')
    # ML WandB implementation
    parser.add_argument('--sweep', help='Run a WandB sweep of hyperparameters', action='store_true')
    parser.add_argument('--sweep_id', type=str, help='Sweep name, to connect with existing WandB sweep agent', metavar="")
    parser.add_argument('--fetch_id', type=str, help='Fetch the model(s) of a performed sweep', metavar="")
    parser.add_argument('--delete_id', type=str, help='Delete .h5 files to retain space from sweep (keep n best files)', metavar="")
    # Current Metadata
    parser.add_argument('-db', '--project_db', help='Implements the current version of the PRIDE metadata MongoDB entry into the current machine', action='store_true')
    # Summary
    parser.add_argument('-S', '--summary', help='In Depth analysis of available metadata- and raw file data', action='store_true')
    # Tutorial
    parser.add_argument('-T', '--tutorial', help='In built tutorial, gathering, extracting and running a test network', action='store_true')
    # DB Move
    parser.add_argument('-M', '--move', help='Move data and entries from one db to another for machine learning testing purposes', type=str, nargs='+', metavar="")
    # Package Test
    parser.add_argument('-pack', '--package_test', help='Test if all packages are loaded and correctly installed', action='store_true')

    return parser


def __package_test():
    try:
        from tutorial.tutorial import full_tutorial
        from summary.run import run_summary
        from storage.db_init import __get_current_pride
        from filter.add_metadata import __add_metadata
        from scraper.run import run_scraper
        from extractor.run import run_extractor
        from filter.run import run_filter
        from filter.add_info import add_info
        from network.run import run_network
        from network.sweep_api import __wandb_sweep
        from storage.move import __move_data
        from extractor.trfp import __get_formatting_software
        exec(f'import {__get_formatting_software(True)}')
        print(f'Success, all packages are installed')
    except Exception as e:
        package = str(e).split("'")[1]
        quit(f'Missing package: {package}')


if __name__ == '__main__':
    try:
        parser = setup_args()
        input_args = parser.parse_args()
    except:
        if any(item in ['-h', '--help'] for item in sys.argv):
            quit()
        else:
            parser.print_help()

    # Python package test!
    if input_args.package_test:
        __package_test()

    from utils.config_inits import get_sconfig, get_econfig

    # MS2AI Tutorial
    if input_args.tutorial:
        from tutorial.tutorial import full_tutorial
        logging.basicConfig(level=get_sconfig()['log_level'].upper())
        full_tutorial()
    # Get local file summary statistics
    if input_args.summary:
        from summary.run import run_summary
        logging.basicConfig(level=get_sconfig()['log_level'].upper())
        run_summary()
    # Get current Projects DB
    if input_args.project_db:
        from storage.db_init import __get_current_pride
        logging.basicConfig(level=get_sconfig()['log_level'].upper())
        __get_current_pride()
    # Add file info to metadata databases
    if input_args.add_metadata_files or input_args.add_metadata_projects:
        from filter.add_metadata import __add_metadata
        logging.basicConfig(level=get_sconfig()['log_level'].upper())
        path = input_args.add_metadata_files if input_args.add_metadata_files else input_args.add_metadata_projects
        db = 'files' if input_args.add_metadata_projects else 'projects'
        __add_metadata(path, db)
    # PRIDE Scraper
    if input_args.scraper:
        from scraper.run import run_scraper
        logging.basicConfig(level=get_sconfig()['log_level'].upper())
        run_scraper()
    # Raw file extractor
    if input_args.extract is not None:
        from extractor.run import run_extractor
        logging.basicConfig(level=get_sconfig()['log_level'].upper())
        run_extractor(input_args.extract, input_args.metadata)
    # Peptide DB filterer
    if input_args.filter is not None:
        from filter.run import run_filter
        logging.basicConfig(level=get_sconfig()['log_level'].upper())
        run_filter(input_args.filter, input_args.binary_filter, input_args.class_count, input_args.class_list, input_args.query)
    # Additional peptide information adder
    if input_args.add_ms1_info is not None or input_args.add_ms2_info is not None:
        from filter.add_info import add_info
        logging.basicConfig(level=get_sconfig()['log_level'].upper())
        add_info(input_args.add_ms1_info, input_args.add_ms2_info)
    # Deep leaning network
    if input_args.network is not None:
        from network.run import run_network
        logging.basicConfig(level=get_sconfig()['log_level'].upper())
        run_network(input_args.network, input_args.test, input_args.test_model, input_args.checkup)
    # WandB sweep api
    if input_args.sweep or input_args.sweep_id or input_args.fetch_id or input_args.delete_id:
        from network.sweep_api import __wandb_sweep
        __wandb_sweep(input_args.sweep_id, input_args.fetch_id, input_args.delete_id)
    # Move peptide files from one database to another for testing or training purposes
    if input_args.move is not None:
        from storage.move import __move_data
        logging.basicConfig(level=get_sconfig()['log_level'].upper())
        if len(input_args.move) != 3:
            quit('Move order not understood! Make sure to read the documentatiton properly before utilizing this feature!')
        __move_data(input_args.move[0], input_args.move[1], input_args.move[2])