import os

import numpy as np
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from tensorflow.keras import Model
from tensorflow.keras.models import load_model

from network.datagenerator import DataGenerator
from network.wandb_api import __wandb_confusion
from storage.db_init import __get_client_dbs
from utils.config_inits import get_metadata_path, get_nconfig


def __pretty_cm(cm, labels, hide_zeroes=False, hide_diagonal=False, hide_threshold=None):
    print(f'Confusion matrix ({round((sum([f[i] for i, f in enumerate(cm)]) / sum([sum(f) for f in cm])) * 100, 3)}%):')
    columnwidth = max([len(x) for x in labels] + [5])  # 5 is value length
    empty_cell = " " * columnwidth
    # Print header
    print("    " + empty_cell*2 + "Predicted")
    print("    " + empty_cell, end=" ")
    for label in labels:
        print("%{0}s".format(columnwidth) % label, end=" ")
    print()
    # Print rows
    for i, label1 in enumerate(labels):
        print("    %{0}s".format(columnwidth) % label1, end=" ")
        for j in range(len(labels)):
            cell = "%{0}.1f".format(columnwidth) % cm[i, j]
            if hide_zeroes:
                cell = cell if float(cm[i, j]) != 0 else empty_cell
            if hide_diagonal:
                cell = cell if i != j else empty_cell
            if hide_threshold:
                cell = cell if cm[i, j] > hide_threshold else empty_cell
            print(cell, end=" ")
        print()


def __get_test_batchsize(test_part, params):
    params['shuffle'] = False
    for i in reversed(range(1, get_nconfig()['batch_size'])):
        if len(test_part) % i == 0:
            params['batch_size'] = i
            break
    return params


def __test_model(partition, labels, params, test_model=False, wandb_confusion=False):
    model_name = f'{params["name"]+"_".join(params["ms_level"]) if not test_model else test_model}.h5'
    if len(partition['testing']) > 0 and os.path.exists(f'{get_metadata_path()}{model_name}'):
        model = load_model(f'{get_metadata_path()}{model_name}')
        params = __get_test_batchsize(partition['testing'], params)

        test_generator = DataGenerator(partition['testing'], labels['testing'], params)
        print(f'Calculating prediction labels', end='\r')
        if params['classification']:
            filtered, db, client = __get_client_dbs(None, 'filtered')
            y_pred = list(model.predict(test_generator, verbose=0).argmax(axis=-1))
            y_real = [filtered.find_one({'_id': f})['label'] for f in partition['testing']][:len(y_pred)]
            __pretty_cm(confusion_matrix(y_real, y_pred), labels=filtered.find_one({'_id': 'index'})['classes'])
            if wandb_confusion:
                __wandb_confusion(y_real, y_pred, filtered.find_one({'_id': 'index'})['classes'])
        else:
            test_accuracy = model.evaluate(test_generator, verbose=0)
            print(f'Test | Loss: {test_accuracy[0]}. R^2: {test_accuracy[1]}')
        quit()
    else:
        quit(f'Test data not available or no trained model exists.')

# params['batch_size'] = len(partition['testing'])
# test_generator = DataGenerator(partition['testing'], labels['testing'], params)
# test_model = Model(model.inputs, model.layers[1].output)
# predictions = test_model.predict(test_generator)
# pred_dict = {f: ff for f, ff in zip(partition['testing'], predictions)}
# np.save('pred.npy', pred_dict)
#
# data = np.load(f'{get_metadata_path()}pred.npy', allow_pickle=True)[()]
# phos = [f for f in labels['testing'] if labels['testing'][f]]
# phos_sum = sum([data[f] for f in phos])
# unmod = [f for f in labels['testing'] if not labels['testing'][f]]
# unmod_sum = sum([data[f] for f in unmod])
#
# plt.set_cmap('hot')
# import matplotlib
# cmap = matplotlib.cm.get_cmap()
# for dat, nam in zip([phos_sum, unmod_sum], ['Phosphorylated', 'Unmodified']):
#     fig, ((ax1, ax2, ax3, ax4), (ax5, ax6, ax7, ax8)) = plt.subplots(nrows=2, ncols=4, figsize=(20, 10))
#     for f in range(8):
#         ax = eval(f'ax{f + 1}')
#         ax.set_xticks([])
#         ax.set_yticks([])
#         ax.imshow(dat[:, :, f])
#     plt.savefig(f'{get_metapath()}{nam}.png')
