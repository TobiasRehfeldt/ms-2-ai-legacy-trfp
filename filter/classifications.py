from collections import Counter, defaultdict
from itertools import chain
from random import shuffle

from utils.config_inits import get_fconfig


def __instrument_classes(list):
    for elements in list:
        if 'q exactive' in elements.lower():
            return 'Q Exactive'
        elif 'ltq' in elements.lower() or 'orbitrap' in elements.lower():
            return 'LTQ Orbitrap'
        elif 'tof' in elements.lower():
            return 'TOF'
    return 'other'


def __peptide_filter(cla, peptides, class_list, binary_class):
    if binary_class is None:
        pep_list = [{**f, **{'label': class_list.index(str(f[cla])), 'name': cla}} for f in peptides if str(f[cla]) in class_list]

    else:
        if cla == 'Sequence' and 'or' in binary_class:  # -f Sequence -b MorC
            chars = binary_class.split('or')
            pep_list = [{**f, **{'label': 1 if any(i in f[cla] for i in chars) else 0, 'name': cla}} for f in peptides if not any(i in f[cla] for i in chars)]

        else:
            pep_list = [{**f, **{'label': 1 if f[cla] == binary_class else 0, 'name': cla}} for f in peptides if f[cla] in class_list]

        class_list = [f'Not {binary_class}', binary_class]

    return pep_list, class_list


def __balance_limiter(pep_list, class_list):
    data_dict = defaultdict(lambda: defaultdict(list))
    [data_dict[f['file']][f['label']].append(f['_id']) for f in pep_list]
    id_dict = defaultdict(list)

    for f in data_dict:
        [shuffle(data_dict[g]) for g in data_dict]
        least_common = min([len(data_dict[f][g]) for g in data_dict[f]])
        for g in data_dict[f]:
            data_dict[f][g] = data_dict[f][g][:least_common]
        if all([len(data_dict[f][g]) == least_common for g in data_dict[f]]) and len(data_dict[f]) == len(class_list):
            id_dict[f] = list(chain(*data_dict[f].values()))

    return [f for f in pep_list if f['_id'] in id_dict[f['file']]]


def __classification(f_class, class_list, binary_class, peptides, filtered):
    pep_list, class_list = __peptide_filter(f_class, peptides, class_list, binary_class)

    if not pep_list:
        quit('No peptides fulfill the requirements, make sure that the right database and class parameters are selected!')

    if get_fconfig()['limit_data']:
        pep_list = __balance_limiter(pep_list, class_list)
    class_counter = Counter([f['label'] for f in pep_list]).most_common()
    [print(f'{class_list[f[0]]}: {f[1]}                            ') for f in class_counter]

    filtered.insert_one({'_id': 'index', 'classes': {str(i): f for i, f in enumerate(class_list)}})
    filtered.insert_many(pep_list)