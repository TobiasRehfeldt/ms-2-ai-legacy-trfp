import os
from collections import defaultdict, Counter
from time import time
import sys
from filter.classifications import __classification
from filter.query import __mongo_query, __peptide_info_query, __file_info_query
from filter.regressions import __regression
from storage.db_init import __get_client_dbs
from utils.config_inits import get_image_path, get_fconfig
import numpy as np

from storage.serialization import read
from utils.common import __validated_input
from extractor.common import __get_ms1_shape


def __set_init(f_class):
    peptides, filtered, db = __get_client_dbs(None, 'peptides', 'filtered')[:3]
    file_info = __get_client_dbs('ms2ai', 'files')[0]
    score = get_fconfig()['score_filter']['score'] if get_fconfig()['score_filter']['apply'] else 0
    class_col = peptides if peptides.find_one({f_class: {'$exists': True}, '_id': {'$ne': 'sizes'}}) else file_info
    ms1shape = __get_ms1_shape()

    return score, file_info, peptides, filtered, db, class_col, ms1shape


def __reset_add_info(filtered):
    if filtered.find_one({'_id': {'$ne': 'index'}}) is not None:
        sample_data = read(f'{get_image_path()}{filtered.find_one({"_id": {"$ne": "index"}})["_id"]}.txt')
        if sample_data[2] or sample_data[3]:
            if __validated_input('Reset additional MS1 and MS2 information from current filtered version?', ['y', 'n'], equals_to='y', default_option='y'):
                os.system(f'python "{sys.argv[0]}" -a1 reset -a2 reset')


def __test_failure(binary_class, class_count, class_list, f_class, class_col):
    if isinstance(class_col.find_one({'_id': {'$ne': 'sizes'}})[f_class], float):
        return True

    if sum([f is not None for f in [binary_class, class_count, class_list]]) != 1:
        a = [f is not None for f in [binary_class, class_count, class_list]]
        inpts = [['-b', '-cc', '-cl'][i] for i, f in enumerate(a) if f]
        if inpts:
            quit(f'{" and ".join(inpts)} are both specified, please only specify one!')
        else:
            quit(f'No class inputs specified, please set -b, -cc or -cl for the filtering.')

    if class_col.find_one({f_class: {'$exists': True}}) is None:
        quit(f'Error: No peptides exists with class "{f_class}"')


def __get_score_percentile(pep_count, peptides, score):
    if get_fconfig()['score_filter']['as_percentile']:
        if pep_count > 1000000:
            scores = [f['Score'] for f in peptides.find({'_id': {'$ne': 'sizes'}}, {"Score": 1, '_id': 0})[:1000000]]
        else:
            scores = [f['Score'] for f in peptides.find({'_id': {'$ne': 'sizes'}}, {"Score": 1, '_id': 0})]
        score = np.percentile(scores, score)
    return score


def __inclusive_rawfiles(binary_class, cla, peps, class_list):
    class_dict = defaultdict(list)
    [class_dict[f['file']].append(f[cla]) for f in peps]  # if not f[cla] in a[f['Raw file']]]

    bin_raws = [f for f in class_dict if set(class_dict[f]) - {binary_class}] if binary_class else \
        [f for f in class_dict if all(item in set(class_dict[f]) for item in class_list)]

    if not bin_raws:
        print('Error: No raw files contain all specified filtering classes! Lower class amount or set "binary_in_all_rawsfiles" to false')
        quit()
    return [f for f in peps if f['file'] in bin_raws]


def __get_class_list(class_count, class_list, binary_cl, cla, peps):
    if class_count is not None:
        class_list = [f[0] for f in Counter([f[cla] for f in peps]).most_common()]
        class_list = class_list[:class_count]
    elif class_list is not None:
        class_list = class_list
    else:
        class_list = [f[0] for f in Counter([f[cla] for f in peps]).most_common()]
        class_list = [f for f in class_list if f == binary_cl or binary_cl not in f]

    return class_list


def run_filter(f_class, binary_class, class_count, class_list, query):
    start = time()
    score, file_info, peptides, filtered, db, class_col, ms1shape = __set_init(f_class)
    __reset_add_info(filtered)
    r = __test_failure(binary_class, class_count, class_list, f_class, class_col)

    pep_count = db.command("collstats", "peptides")["count"]
    print(f'Total peptides: {"{:,}".format(pep_count)}')
    time_pr_pep = 1.75e-05
    print(f'Estimated runtime: {round(pep_count * (1 - (score / 100)) * time_pr_pep, 2)}s '
                          f'or {round(pep_count * (1 - (score / 100)) * time_pr_pep / 60, 2)}m')
    print(f'Calculating Score percentiles - Time: {round(time()-start,2)}                                    ', end='\r')
    score = __get_score_percentile(pep_count, peptides, score)

    print(f'Retrieving list of valid peptides - Time: {round(time()-start,2)}                                    ', end='\r')
    peptide_query, file_info_query = __mongo_query(query, peptides, file_info, score)

    peps = __peptide_info_query(peptides, peptide_query, ms1shape)
    peps = __file_info_query(peptides, peptide_query, class_col, file_info_query, file_info, f_class, peps)

    if not r:
        class_list = __get_class_list(class_count, class_list, binary_class, f_class, peps)

    # Update peptide list to only those wtih raw files where all classes exist
    if get_fconfig()['binary_in_all_rawsfiles']:
        print(f'Removing raw files without required classes - Time: {round(time()-start,2)}                                    ', end='\r')
        peps = __inclusive_rawfiles(binary_class, f_class, peps, class_list)

    print(f'Writing peptides to filtered DB - Time: {round(time()-start,2)}                                    ', end='\r')
    # The program is now ready to start creating a new filtered version, so it drops the old
    filtered.drop()
    if r:
        __regression(f_class, peps, filtered, db)

    else:
        __classification(f_class, class_list, binary_class, peps, filtered)

    print(f'Time elaborated: {round((time()-start) / 60, 3)}min '
          f'| Per peptides: {"{:e}".format(np.mean([((time()-start) / len(peps)), (time()-start) / db.command("collstats", "peptides")["count"]]))} seconds')


# -f Modifications -b "Phospho (STY)" -a1 "m/z" "Retention time" "instrument" "organism" -a2 "m/z*" "Retention time*" "instrument" "organism" "Charge" "fragmentation" "fragmentation energy*"