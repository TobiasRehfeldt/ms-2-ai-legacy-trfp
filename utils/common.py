from os.path import splitext
import re
from time import sleep
from urllib.error import URLError
from urllib.request import urlopen

from utils.log import ms2ai_log


def __validated_input(prompt, valid_values, equals_to=None, default_option=None, show_options=True):
    def __to_default(input_value):
        if default_option is not None and input_value == '':
            return default_option
        else:
            return input_value

    value = input(f'{prompt} {" / ".join(valid_values if default_option is None else [f if f != default_option else f"[{f.upper()}]" for f in valid_values])}: ') if show_options else input(prompt)
    value = __to_default(value)
    while value not in valid_values:
        value = input(f'{prompt} {" / ".join(valid_values if default_option is None else [f if f != default_option else f"[{f.upper()}]" for f in valid_values])}: ') if show_options else input(prompt)
        value = __to_default(value)

    if equals_to is not None:
        return value == equals_to
    else:
        return value


def all_string_match(string, pattern, overlap=False):
    if overlap:
        return [m.start() for m in re.finditer('(?=%s)(?!.{1,%d}%s)' % (pattern, len(pattern) - 1, pattern), string)]
    else:
        return [m.start() for m in re.finditer(pattern, string)]


def match_ext(path, multi_exts=False):
    if bool(multi_exts):
        for ext in multi_exts:
            if path.endswith(ext):
                return path[-len(ext):]
    return splitext(path)[1]


def __url_open_attempts(url, attempst=10):
    project = [f for f in url.split('/') if 'PXD' in f][0]
    for attempt in range(attempst):
        try:
            return urlopen(url).read()

        except URLError as e:
            ms2ai_log(f'{project}: {e}, Likely cause: Rate throttling', 'debug')
            sleep(2)

        except Exception as e:
            # traceback.print_exc()
            ms2ai_log(f'{project}: {e}', 'info')
            raise


def __urlretrieve_show_progress(block_num, block_size, total_size):
    global pbar
    import progressbar
    pbar = progressbar.ProgressBar(maxval=total_size)
    pbar.start()

    downloaded = block_num * block_size
    if downloaded < total_size:
        pbar.update(downloaded)
    else:
        pbar.finish()
        pbar = None


def _get_sizes(path, filelist):
    import os
    from glob import glob
    a = {}
    for f in filelist:
        a[f] = sum([os.path.getsize(f) for f in glob(f'{path}{f}')])/1024/1024/1024
    print(a, sum(a.values()))
# _get_sizes('/data/tobias/PXD*/*/', ['evidence.txt', 'file.raw', '*x*.txt', 'file.mzML.gz', 'mzML.txt'])