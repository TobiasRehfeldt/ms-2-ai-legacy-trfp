from math import ceil

import numpy as np

from extractor.common import __get_lower_bound, __import_table
from extractor.imgs.create_pngs import __pep_png_image
from extractor.filters import peptide_filters
from extractor.imgs.ms2_info import __ms2_bin
from storage.db_init import __get_client_dbs
from storage.serialization import write, __create_file
from utils.config_inits import get_image_path, get_econfig, get_sconfig
from utils.log import __internal_print


def __get_neighbourhood(range_lists, pep, window):
    mz_lower = __get_lower_bound(range_lists[0], pep['m/z']) - window[0]
    mz_upper = __get_lower_bound(range_lists[0], pep['m/z']) + window[0]+1
    rt_lower = __get_lower_bound(range_lists[1], pep['Retention time']) - window[1]
    rt_upper = __get_lower_bound(range_lists[1], pep['Retention time']) + window[1]+1

    return mz_lower, mz_upper, rt_lower, rt_upper


def __get_ms1_info(image, rt_lower, rt_upper, mz_lower, mz_upper):
    ms1info = image[rt_lower:rt_upper, mz_lower:mz_upper, :]
    ms1size = ms1info.shape

    return ms1info, ms1size


def __get_ms2_info(ms2_mzML, ms2scan):
    ms2_binning = get_econfig()['ms2_binning']
    # Draw neighbourhood from large image
    if ms2_binning['apply']:
        # Binning the ms2 into 1 of 4 different binning methods
        (ms2info, ms2size) = __ms2_bin(ms2_mzML, ms2scan, ms2_binning)

    else:
        # Draw ms2 information from mzml file
        mz_info = np.array([f for f in ms2_mzML[str(ms2scan)]['m/z']])
        mz_info = mz_info /ms2_binning['fixed_mz_length']

        int_info = np.array([f for f in ms2_mzML[str(ms2scan)]['intensity']])
        int_info = int_info / max(int_info)

        ms2info = np.array([mz_info, int_info])
        ms2size = ms2info.shape

    return ms2info, ms2size


def __append_to_metadata(accnr, ms1shape, ms2shape, rows, db_name, columns, peptides):
    # Write metadata to MongoDB
    metadata = {'_id': db_name, 'accession': accnr, 'identifier': f"{accnr}/{rows['Raw file']}", 'ms1shape': ms1shape, 'ms2shape': ms2shape}
    for f in columns:
        metadata[str(f.replace('.', ''))] = rows[f]

    peptides.insert_one(metadata)


def __pep_txt_image(image, ms2_mzML, filepath, accnr, filename, window, range_lists, multiprocessing):
    __create_file(f'{get_image_path()}{get_sconfig()["db_name"]}/{accnr}/{filename}')
    df = __import_table(file=f'{filepath}{get_econfig()["maxquant_file"]}')
    df.reset_index(drop=True, inplace=True)
    ms_extraction = get_econfig()['extract_ms']
    peptides, db, client = __get_client_dbs(None, 'peptides')
    for i, pep in df.iterrows():
        if i % ceil(len(df) * 0.01) == 0:
            __internal_print(f'Creating LC-MS Peptide Representation: {round(i / len(df) * 100)}%', multiprocessing)

        # Find the closest and highest intensity scan in msms scan list
        ms2scan = pep['mzML scan']
        pep_db_name = f'{get_sconfig()["db_name"]}/{accnr}/{filename}/{ms2scan}'

        # Retrieve neighbourhood numbers
        mz_lower, mz_upper, rt_lower, rt_upper = __get_neighbourhood(range_lists, pep, window)

        # Go through the filters
        if peptide_filters(pep, [rt_lower, rt_upper, mz_lower, mz_upper], image, pep_db_name, ms_extraction):
            continue

        # Retrieve ms info.
        ms1info, ms1shape = __get_ms1_info(image, rt_lower, rt_upper, mz_lower, mz_upper) if 'ms1' in ms_extraction else [False, None]
        ms2info, ms2shape = __get_ms2_info(ms2_mzML, ms2scan) if 'ms2' in ms_extraction else [False, None]

        # Save image as into storage, and saves the metadata in db
        write([ms1info, ms2info, False, False], f'{get_image_path()}{pep_db_name}.txt')
        __append_to_metadata(accnr, ms1shape, ms2shape, pep, pep_db_name, list(pep[pep.notnull()].axes[0]), peptides)

        # Save peptide images as png files
        if get_econfig()['save_images_as_png']:
            # range_lists[0][mz_lower], range_lists[0][mz_upper], range_lists[1][rt_lower], range_lists[1][rt_upper]
            __pep_png_image(ms1info[::-1], pep_db_name)
