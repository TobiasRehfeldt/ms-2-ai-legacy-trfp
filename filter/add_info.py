from copy import deepcopy
from itertools import chain
from time import time

import numpy as np

from filter.encodings import __one_hot, __linear_relationship, __norm_value
from storage.db_init import __get_client_dbs
from utils.common import __validated_input
from utils.config_inits import __get_path
from storage.serialization import read, write


def __db_init():
    filtered, (file_info, project_info) = __get_client_dbs(None, 'filtered')[0], __get_client_dbs('ms2ai', 'files', 'projects')[:2]
    filtered_raws = filtered.find({'_id': {'$ne': 'index'}}).distinct('file')
    filtered_accs = filtered.find({'_id': {'$ne': 'index'}}).distinct('accession')
    pep_count = filtered.count_documents({})
    if pep_count == 0:
        quit('No filtered collection exists, filter the peptide and re-run')

    return filtered, file_info, project_info, filtered_raws, filtered_accs, pep_count



def __remove_and_normalize(index_dict, args):
    # Remove single class features.
    for f in list(index_dict.keys()):
        if isinstance(index_dict[f], list) and len(index_dict[f]) < 2:
            print(f'Warning: "{f}" has {len(index_dict[f])} class{"es" if len(index_dict[f]) == 0 else ""}, and will not be added!')
            del index_dict[f]

    # Give max values for normalization dictionary
    [index_dict.update({f: max([int(g) for g in index_dict[f]])}) for f in [f for f in index_dict if '*' in f]]

    # Add a catch-all if a peptide do not have the required information
    for f in index_dict:
        if isinstance(index_dict[f], list):
            if isinstance(index_dict[f][0], str):
                index_dict[f].append('other')  # Catch-all for one-hot encoded is None
            elif isinstance(index_dict[f][0], int) and 0 not in index_dict[f]:
                index_dict[f].append(0)  # Catch-all for continuous classes is 0

    return index_dict, args


def __compile_index(adds, filtered, file_info, project_info, filtered_raws, filtered_accs):
    index_dict = {}
    lookup_dict = {}
    # Iterate through the arguments, and construct a dictionary of the index values
    for i, arg in enumerate(deepcopy(adds)):
        if not arg:
            continue

        for key in arg:
            key_ns = key.replace('*', '')

            if filtered.find_one({key_ns: {'$exists': True}}):
                index_dict.update({key: filtered.distinct(key_ns)})
                lookup_dict[key_ns] = filtered
            elif file_info.find_one({'identifier': {'$in': filtered_raws}, key_ns: {'$exists': True}}):
                index_dict.update({key: file_info.find({'identifier': {'$in': filtered_raws}}).distinct(key_ns)})
                lookup_dict[key_ns] = file_info
            elif project_info.find_one({'accession': {'$in': filtered_accs}, key_ns: {'$exists': True}}):
                index_dict.update({key: project_info.find({'accession': {'$in': filtered_accs}}).distinct(key_ns)})
                lookup_dict[key_ns] = project_info
            elif key != 'reset':
                if __validated_input(f'"{key_ns}" not available. Proceed without?', ['y', 'n'], equals_to='n', default_option='y'):
                    [print(f'{ff} specific informations: {list(f.find_one().keys())}') for f, ff in zip([filtered, file_info], ['Peptides', 'File'])]
                    quit()
                else:
                    adds[i].remove(key)

    index_dict, adds = __remove_and_normalize(index_dict, adds)

    lookup_funcs = {}
    for key in index_dict:
        # One hot encoding of class variables!
        if isinstance(index_dict[key], list) and (isinstance(index_dict[key][0], str) or index_dict[key][0] is None):
            lookup_funcs[key] = __one_hot
        # A list of integers such as Charge that has linear relationship and is not normalized
        elif isinstance(index_dict[key], list) and isinstance(index_dict[key][0], (float, int)):
            lookup_funcs[key] = __linear_relationship
        # A normalized value
        else:
            lookup_funcs[key] = __norm_value

    return index_dict, lookup_dict, lookup_funcs, adds[0], adds[1]


def __flatten_list(x):
    return_list = []
    for f in x:
        if not isinstance(f, list):
            f = [f]
        return_list = return_list + list(f)
    return return_list


def __add_ms_info(ms, data, db_entry, ms_args, index_dict, lookup_funcs):
    if 'reset' in ms_args:
        data[ms+1] = False
        return data

    ms_info = [lookup_funcs[arg](arg.replace('*', ''), db_entry, index_dict, arg) for arg in ms_args if arg in index_dict]
    data[ms+1] = __flatten_list(ms_info) if ms_info else False
    return data


def __add_artifact(data, db_entry):
    if db_entry['label'] == 1:
        a = deepcopy(data[0])
        a[2:5, 85:90, 0:3] = 1
        data[0] = a
    return data


def add_info(ms1_adds, ms2_adds):
    filtered, file_info, project_info, filtered_raws, filtered_accs, pep_count = __db_init()
    index_dict, lookup_dict, lookup_funcs, ms1_adds, ms2_adds = __compile_index([ms1_adds, ms2_adds], filtered, file_info, project_info, filtered_raws, filtered_accs)
    print(f'Estimated time: {round(0.01 * pep_count, 2)}sec / {round(0.01 * pep_count / 60, 2)}min')

    st = time()
    db_list = list(filtered.find({'_id': {'$ne': 'index'}}, {**{'identifier': 1, 'accession': 1, 'label': 1}, **{f: 1 for f in lookup_dict}}))
    [f.update({g: lookup_dict[g].find_one({'identifier': f['identifier'], g: {'$exists': True}} if lookup_dict[g].name == 'files' else
                                          {'accession': f['accession'], g: {'$exists': True}})[g]}) for f in db_list for g in lookup_dict if g not in f]
    for i, db_entry in enumerate(db_list):
        if i % 100 == 0 or i == pep_count:
            print(f'Progress: {i}/{pep_count}', end='\r')
        data = read(f'{__get_path()}images/{db_entry["_id"]}.txt')
        if ms1_adds is not None:
            data = __add_ms_info(1, data, db_entry, ms1_adds, index_dict, lookup_funcs)
            # data = __add_artifact(data, db_entry)
        if ms2_adds is not None:
            data = __add_ms_info(2, data, db_entry, ms2_adds, index_dict, lookup_funcs)
        write(data, f'{__get_path()}images/{db_entry["_id"]}.txt')
    print(f'Total time: {round(time() - st, 2)}. Per file: {round((time() - st) / pep_count, 5)}')
    print(f'Done!')