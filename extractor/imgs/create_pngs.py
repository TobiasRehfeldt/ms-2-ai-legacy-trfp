import os
import numpy as np
import matplotlib.pyplot as plt

from storage.db_init import __get_client_dbs
from utils.config_inits import get_image_path


def __full_png_image(image, filepath, resolution, filename, accnr):
    file = __get_client_dbs('ms2ai', 'files')[0].find_one({'Raw file': filename, 'accession': accnr})
    listnames = ['Mean', 'Min', 'Max', 'Collapsed']
    for i in range(4):
        fullimage = image[:, :, i]
        titleofplot = listnames[i]

        inf_list = [file['ms1 min m/z'], file['ms2 max m/z'], file['min rt'], file['max rt']]
        plt.figure(figsize=(12, 8))
        plt.imshow(fullimage, extent=inf_list, aspect='auto')
        plt.tight_layout()
        plt.title(titleofplot)
        plt.xlabel('m/z', fontsize=12)
        plt.ylabel('Retention Time', fontsize=12)
        plt.axis(inf_list)
        plt.tight_layout()
        plt.savefig(f'{filepath}{str(resolution["x"])}x{str(resolution["y"])}-{titleofplot}.png')


def __pep_png_image(subimage, filename):
    newimage = subimage[:, :, 0]
    newimage = np.ma.masked_equal(newimage, 0)

    fig = plt.figure()
    fig.set_size_inches(2, 2)  # (mzupper - mzlower)/100,(rtupper - rtlower)/100)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    plt.set_cmap('hot')
    ax.imshow(newimage, aspect='equal')
    plt.savefig(f'{get_image_path()}{filename}.png')
