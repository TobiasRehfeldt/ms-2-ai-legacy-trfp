import os
import re
from shutil import copyfile
from zipfile import ZipFile

import numpy as np

from extractor.file_metadata import __rawfile_metadata
from extractor.iter_raw import __iterate_rawfiles
from storage.serialization import __create_file
from utils.config_inits import __get_path, get_econfig, __remove_used
from utils.log import ms2ai_log


def __pride_iter(zips, pre_mq_info, accnr, rawfiles, multiprocessing):
    try:
        (zip_rawfiles, df) = (os.listdir(f'{__get_path()}{accnr}'), None) if pre_mq_info else __rawfile_metadata(zips, accnr, multiprocessing)
        # Finding which zip files matches the naming schemes
        foundrawfiles = np.array([(f, f.split('/')[-1].split('.')[0]) for f in rawfiles if f.split('/')[-1].split('.')[0] in zip_rawfiles])
        rawfile_links = foundrawfiles[:, 0]
        rawfile_names = foundrawfiles[:, 1]

        for filename in rawfile_names:
            filepath = f'{__get_path()}{accnr}/{filename}/'
            if get_econfig()['acquire_only_new'] and os.path.exists(f'{filepath}mzML.txt'):
                continue
            __iterate_rawfiles(filename, filepath, accnr, rawfile_links, df, multiprocessing)

    except KeyboardInterrupt:
        quit()
    except Exception as e:
        ms2ai_log(f'{accnr}(zipfile), {e.__class__.__name__}: {e}', 'info')
        pass


def __local_iter(file, dirpath, accnr, multiprocessing):
    try:
        with ZipFile(f'{dirpath}{file}', 'r') as zip_file:
            rawfiles, raw_df = __rawfile_metadata(zip_file, accnr, multiprocessing)
        __remove_used(f'{dirpath}working.zip', True)

        rawfiles = [f for f in rawfiles if os.path.exists(f'{dirpath}{f}.raw') or f in dirpath]
        for raw in rawfiles:
            filepath = f'{__get_path()}{accnr}/{raw}/'
            __create_file(filepath)
            if not any(item in os.listdir(filepath) for item in ['file.raw', 'file.mzML.gz', 'mzML.txt']):
                copyfile(f'{dirpath}{raw}.raw', f'{filepath}file.raw')
            __iterate_rawfiles(raw, filepath, accnr, rawfiles, raw_df, multiprocessing)

    except KeyboardInterrupt:
        quit()
    except Exception as e:
        ms2ai_log(f'{accnr}{dirpath[re.search(accnr, dirpath).end():].replace("/", "")}/{file} ZipFile error: {e}', 'info')
        pass


def _get_sizes(path, filelist):
    import os
    from glob import glob
    a = {}
    for f in filelist:
        a[f] = sum([os.path.getsize(f) for f in glob(f'{path}{f}')])/1024/1024/1024
    print(a, sum(a.values()))