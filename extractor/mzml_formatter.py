import os
import subprocess
from time import time

from extractor.trfp import __get_formatting_software, __install_conda_thermo, __install_docker_thermo
from utils.config_inits import __get_path, __remove_used, get_econfig
from utils.log import __internal_print, ms2ai_log


def __run_conda_thermo(relpath, accnr, filename):
    try:
        conda_path = subprocess.check_output('which conda', shell=True).decode('ascii')[:-6]
        peak_picking = '' if get_econfig()['thermo_peak_picking'] else '-p'
        timer = time()
        os.system(f'mono "{conda_path}ThermoRawFileParser.exe"'           # Run TRFP from conda path
                  f'      -i="{relpath}{accnr}/{filename}/file.raw" '   # Input path with raw file
                  f'      -o="{relpath}{accnr}/{filename}/" '           # Output path without file name
                  f'      -f=2 -g -m=0 {peak_picking} -l=0')            # .txt metadata file, mzML.gz, peak_picking, and muted.
        # print(time() - timer)
    except KeyboardInterrupt:
        os.remove(f'{relpath}{accnr}/{filename}/file.mzML.gz')


def __run_docker_thermo(relpath, accnr, filename):
    try:
        peak_picking = '' if get_econfig()['thermo_peak_picking'] else '-p'
        os.system(f'docker run '
                    f'-v "{relpath[:-1]}:/data_input" '                 # Set path as /data_input
                    f'veitveit/trp ThermoRawFileParser '                # Docker image to run
                    f'-i="/data_input/{accnr}/{filename}/file.raw" '    # Input path with raw file
                    f'-o="/data_input/{accnr}/{filename}/" '            # Output path without file name
                    f'-f=2 -g -m=0 {peak_picking} -l=0')                # .txt metadata file, mzML.gz, peak_picking, and muted.
    except:
        print(f'Cannot run docker image. Check that docker is running and working or that the RAW file is not corrupted')
        os.remove(f'{relpath}{accnr}/{filename}/file.mzML.gz')


def __mzML_formatter(accnr, filename, filepath, multiprocessing):
    def __file_exists(name):
        return os.path.exists(f'{filepath}{name}')

    __internal_print('Formatting file to mzML', multiprocessing)
    relpath = __get_path() if (__get_path().startswith('/') or ':' in __get_path()) else f'{os.getcwd()}/{__get_path()}'

    # If rawfile exists and either 'fragmentation' doesnt appear in the df, or the future files doesnt exists
    if __file_exists('file.raw') and not (__file_exists('file.mzML.gz') or __file_exists('mzML.txt')):
        if __get_formatting_software(multiprocessing) == 'conda':
            __run_conda_thermo(relpath, accnr, filename)

        elif __get_formatting_software(multiprocessing) == 'docker':
            __run_docker_thermo(relpath, accnr, filename)

        else:
            ms2ai_log('format software not specified in config.json. Choices are "conda" or "docker"', 'error')
            return quit()

        if os.path.exists(f'{filepath}file.mzML.gzip'):
            os.rename(f'{filepath}file.mzML.gzip', f'{filepath}file.mzML.gz')  # Using .gz instead of .gzip
        __remove_used(f'{filepath}file.raw')