import traceback
import numpy as np
from pride_api.get_urls import __get_zip_files, __get_extensions, __get_loose_files, __get_files_api
from pride_api.keys import __maxquant_file_names
from storage.compressions import __get_compressed_file
from utils.config_inits import get_sconfig
from utils.log import ms2ai_log


def __maxquant_fileinfo(pride_entry, zipfiles):
    if pride_entry['maxquant'] and zipfiles:
        smallest_zip = sorted(zipfiles, key=lambda x: x[1], reverse=True)[0][0]
        size = sorted(zipfiles, key=lambda x: x[1], reverse=True)[0][1]

        if size*1e-9 > get_sconfig()["max_zipfile_size"]:
            return {'maxquant_files': f'error: zipfile size of {round(size * 1e-9, 2)}gb exceeds limit of {get_sconfig()["max_zipfile_size"]}gb.'}

        try:
            zip_file = __get_compressed_file(smallest_zip)
            files_in_zip = set([files.orig_filename.split('/')[-1] for files in zip_file.filelist
                                           if files.orig_filename.split('/')[-1] in __maxquant_file_names()
                                           or 'xml' in files.orig_filename.split('/')[-1]])
            return {'maxquant_files': files_in_zip}

        except:
            return {'maxquant_files': 'error: unknown zipfile error'}

    elif pride_entry['maxquant'] and not zipfiles:
        return {'maxquant_files': 'error: zipfile not found'}

    return {}


def __metadata_files(pride_entry):
    # Get the extensions
    files_api = __get_files_api(pride_entry['accession'])
    pride_entry.update(__get_extensions(files_api))

    # Retrieve the zip and loose files
    zipfiles = __get_zip_files(files_api, get_size=True)
    loose_files = __get_loose_files(files_api)

    if loose_files:
        # Get the maxquant files from loose files
        pride_entry.update({'maxquant_files': [f.split('/')[-1] for f in loose_files]})
    elif zipfiles:
        # Get the maxquant files from the smallest zipfile
        pride_entry.update(__maxquant_fileinfo(pride_entry, zipfiles))
    else:
        pride_entry.update({'maxquant_files': 'error: no maxquant files found'})


    return pride_entry