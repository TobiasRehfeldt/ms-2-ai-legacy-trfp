from collections import defaultdict

import numpy as np

from extractor.common import __get_lower_bound
from storage.db_init import __get_client_dbs
from utils.log import __internal_print


def __iterate_mzml(ms1_mzML, filename, accnr, range_lists, multiprocessing):
    file = __get_client_dbs('ms2ai', 'files')[0].find_one({'Raw file': filename, 'accession': accnr})
    ms1_pixel_info = defaultdict(list)
    filemax_int = np.log(file['ms1 max intensity'])

    len_percentage = int(len(ms1_mzML) * 0.01)
    for i, f in enumerate(ms1_mzML):
        if i % len_percentage == 0:
            __internal_print(f'Creating LC-MS Representation: {round(i / len(ms1_mzML) * 100)}%', multiprocessing)
        ms1_values = ms1_mzML[f]
        rt_pixel = __get_lower_bound(range_lists[1], ms1_values['scan time'])

        for intensity, mz in zip(ms1_values['intensity'], ms1_values['mz']):
            mz_pixel = __get_lower_bound(range_lists[0], mz)
            intensity_val = np.log(max(intensity, 1)) / filemax_int
            pixel = (mz_pixel, rt_pixel)
            ms1_pixel_info[pixel].append(intensity_val)
    return ms1_pixel_info


def __create_image_data(ms1_mzML, filename, accnr, resolution, range_lists, multiprocessing):
    ms1_pixel_info = __iterate_mzml(ms1_mzML, filename, accnr, range_lists, multiprocessing)
    max_collapsed = max([len(ms1_pixel_info[f]) for f in ms1_pixel_info])
    image = np.zeros((resolution['y'], resolution['x'], 4))
    for (x, y), value in ms1_pixel_info.items():
        if x < resolution['x']:
            image[y, x] = [np.mean(value), min(value), max(value), len(value) / max_collapsed]

    return image

