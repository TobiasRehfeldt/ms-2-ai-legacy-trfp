import math
from collections import Counter
from itertools import chain
from pathlib import Path

from utils.common import __validated_input
from pymongo import MongoClient

from utils.config_inits import __get_path, get_image_path


def convert_size(size_bytes):
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return "%s %s" % (s, size_name[i])


def __print_option(first, last, counted_list):
    [print(f'({i + first}) {f[0]}: {f[1]} occurances') for i, f in enumerate(counted_list[first:last])]


def __print_inital_info():
    client = MongoClient()
    cursor = client.list_databases()

    databases = []
    for db in cursor:
        current_db = client[db['name']]
        current_db_name = db['name']
        if 'peptides' in current_db.list_collection_names():
            storage_db = convert_size(db['sizeOnDisk'])
            storage_dir = convert_size(sum(f.stat().st_size for f in Path(f'{get_image_path()}{current_db_name}/').glob('**/*') if f.is_file()))
            peptide = current_db.command("collstats", "peptides")["count"]
            try:
                filtered = current_db.command("collstats", "filtered")["count"]
            except:
                filtered = 0
            databases.append(current_db_name)
            print(
                f'DB: {current_db_name}. Storage (DB): {storage_db}, Storage (Dir): {storage_dir}. Total peptides: {peptide}, of which {filtered} are currently in a filtered version')

    current_db = MongoClient()['ms2ai']
    storage = round(current_db.command("collstats", "projects")['size'] * 1e-9, 4)
    metadata = current_db.command("collstats", "projects")['count']
    databases.append('projects')
    print(f'DB: projects. Storage: {storage} gb. Total project entries: {metadata}. Extend for more information on metadata')

    return __validated_input('\nChoose DB to extend: ', databases, show_options=False)


def __specific_info(collection, query, database_name, query_name=None):
    def __return(database_name):
        if database_name == 'projects':
            return __extended_pride_info()
        else:
            return __extended_peptide_info(database_name)

    if query_name is None:
        query_name = query

    print(f'Calculating!', end="\r")
    count_aggregation = list(collection.aggregate([
        {'$group': {'_id': f'${query}', 'count': {'$sum': 1}}}]))
    counted_list = sorted([list(f.values()) for f in count_aggregation], key=lambda x: x[1], reverse=True)
    print(f'Done!', end="\r")

    start_value = 0
    end_value = 10
    __print_option(start_value, end_value, counted_list)
    possible_options = list(chain.from_iterable([['', 'next'], [str(f) for f in range(0, end_value)]]))
    next_or_back = __validated_input(f'\nOptions: Value for projects with given {query_name}. '
                                     f'Press enter to go back. Type "next" to get next elements in the list! ',
                                     possible_options, show_options=False)
    while next_or_back == 'next':
        start_value = end_value
        end_value = end_value + 10
        __print_option(start_value, end_value, counted_list)
        possible_options = list(chain.from_iterable([['', 'next'], [str(f) for f in range(0, end_value)]]))
        next_or_back = __validated_input(f'\nOptions: Value for projects with given {query_name}. '
                                         f'Press enter to go back. Type "next" to get next elements in the list! ',
                                         possible_options, show_options=False)

    if next_or_back == '':
        return __return(database_name)

    else:
        relevant_projects = collection.find({query: counted_list[int(next_or_back)][0]}).distinct('accession')
        print(f"Projects: {', '.join(relevant_projects)}")
        input('Hit enter to go back!')
        return __return(database_name)


def __extended_pride_info():
    data = MongoClient()['ms2ai']['projects']
    print(f'\npride choosen! See list for additional info')
    info_options = ['Organisms', 'Organism parts', 'Diseases', 'MS Instruments', 'Softwares', 'Quantification Methods', 'PTMs', 'Go back!']
    actual_name_list = ['organisms', 'organismParts', 'diseases', 'instruments', 'softwares', 'quantificationMethods', 'identifiedPTMStrings']

    [print(f'({i}) {f}') for i, f in enumerate(info_options)]
    requested_info = __validated_input(f'Request information on: ', [str(f) for f in range(len(info_options))], show_options=False)

    if int(requested_info) + 1 == len(info_options):
        print(f'\nReturning to DB selection!')
        run_summary()

    __specific_info(data, actual_name_list[int(requested_info)], 'projects', query_name=info_options[int(requested_info)])
    print()


def __resolution_info(peptide_data, database_name):
    print(f'\nMS1 Resolution: {peptide_data.find_one()["ms1size"]} \nMS2 Resolution: {peptide_data.find_one()["ms2size"]}')
    input('Press enter to go back!')
    return __extended_peptide_info(database_name)


def __extended_peptide_info(database_name):
    db = MongoClient()[database_name]
    peptides = db['peptides']
    print(f'\n{database_name} chosen! See list for additional information')
    info_options = ['Image resolutions', 'Charge states', 'Sequences', 'Sequence lengths', 'Proteins', 'Modifications (PTMs)', 'Go back!']
    actual_name_list = ['', 'Charge', 'Sequence', 'Length', 'Proteins', 'Modifications']
    [print(f'({i}) {f}') for i, f in enumerate(info_options)]
    requested_info = __validated_input(f'Request information on:', [str(f) for f in range(len(info_options))], show_options=False)
    if int(requested_info) + 1 == len(info_options):
        print(f'\nReturning to DB selection!')
        run_summary()
    elif requested_info == '0':
        __resolution_info(peptides, database_name)
    else:
        __specific_info(peptides, actual_name_list[int(requested_info)], database_name, query_name=info_options[int(requested_info)])
    print()


def run_summary():
    requested_db = __print_inital_info()

    if requested_db == 'projects':
        __extended_pride_info()
    else:
        __extended_peptide_info(requested_db)