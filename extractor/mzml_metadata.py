import os
from collections import defaultdict

import numpy as np
import pandas as pd
from simplejson import loads

from extractor.common import __import_table
from extractor.imgs.ms2_info import __find_ms2_scan, __ms2_scan_name
from extractor.xml_parser import __flatten_dict
from storage.db_init import __get_client_dbs, __insert_or_update
from storage.serialization import read
from utils.config_inits import get_econfig, __remove_used
from utils.exceptions import mzMLScanError
from utils.log import __internal_print


def __norm_and_filters(max_rt, min_rt, min_mz, max_mz, data_length, df):
    norm_rt = [pep['Retention time'] / (max_rt - min_rt) for index, pep in df.iterrows()]
    norm_mz = [pep['m/z'] / (max_mz - min_mz) for index, pep in df.iterrows()]
    step_size = (max_rt - min_rt) / data_length
    return norm_mz, norm_rt, step_size


def __mzML_metadata(filepath):
    meta = {}
    if os.path.exists(f'{filepath}file-metadata.json'):
        mzML_metadata = loads(open(f'{filepath}file-metadata.json').read())
        [meta.update({g['name']: g['value']}) for f in list(mzML_metadata.keys())[1:] for g in mzML_metadata[f]]
        __remove_used(f'{filepath}file-metadata.json')
    return meta


def __append_to_df(mzML, df, filepath, mzML_df_keys):
    df['mzML scan'] = [str(__find_ms2_scan(f, mzML, __ms2_scan_name(df))) for i, f in df.iterrows()]

    # Remove bad mzML files
    if False in [f in mzML['ms2'] for f in df['mzML scan']]:
        os.remove(f'{filepath}mzML.txt')
        raise mzMLScanError

    mzML_info = defaultdict(list)

    [mzML_info[g].append(mzML['ms2'][str(f)][g] if g in mzML['ms2'][str(f)] else None) for f in df['mzML scan'] for g in mzML_df_keys]
    norm_mz = list(np.array(df['m/z']) / mzML['file']['ms1']['max m/z'])
    norm_rt = list(np.array(df['Retention time']) / mzML['file']['max rt'])
    mz_diff = list(np.absolute(np.around(np.array(mzML_info['precursor m/z']) - np.array(df['m/z']), 2)))
    mzML_info.update({'normalized m/z': norm_mz, 'normalized retention time': norm_rt, 'mzML m/z difference': mz_diff})

    for f in mzML_info:
        df[f] = mzML_info[f]

    pd.DataFrame.to_csv(df, f'{filepath}{get_econfig()["maxquant_file"]}', index=False)


def __mzML_info(filepath, filename, accnr, multiprocessing):
    __internal_print('Computing run statistics', multiprocessing)
    mzML = read(f'{filepath}mzML.txt')
    # Append file specific information to the file info db
    meta = __mzML_metadata(filepath)
    insert_info = {**{'identifier': f'{accnr}/{filename}', 'accession': accnr, 'Raw file': filename},
                   **__flatten_dict(mzML['file'], keep_parents=True, sep=' '),
                   **meta}
    __insert_or_update(__get_client_dbs('ms2ai', 'files')[0], insert_info, ['identifier'])

    # Append fragmentation and normalized values to the df
    mzML_df_keys = max([list(mzML['ms2'][f])[3:] for f in mzML['ms2']], key=len)
    df = __import_table(file=f'{filepath}{get_econfig()["maxquant_file"]}')
    if not all(item in mzML_df_keys for item in df.columns):
        __append_to_df(mzML, df, filepath, mzML_df_keys)


def __mzML_keys():
    return ['min rt', 'max rt', 'rt step size', 'min mz filter', 'max mz filter', 'ms1 min m/z',
            'ms1 max m/z', 'ms1 max intensity', 'ms2 min m/z', 'ms2 max m/z', 'ms2 max intensity']