import numpy as np

from utils.common import __validated_input


def __mongo_query(query, peptides, file_info, score):
    peptide_query = {'_id': {'$ne': 'sizes'}, 'Score': {'$gt': score}}
    file_info_query = {}

    operators = ['=', '<', '>']
    if query is not None:
        for f in query:
            # Finding get operator, key and value for each query
            operator = operators[[i for i, op in enumerate(operators) if op in f][0]]
            key, value = [f.strip() for f in f.split(operator)]

            # Using the key, we find out if it comes from peptides or file info, and what type it has
            query_col = peptides if peptides.find_one({'_id': {'$ne': 'sizes'}, key: {'$exists': True}}) else file_info
            if not query_col.find_one({key: {'$exists': True}}):
                if __validated_input(f'"{key}" not available. Proceed without?', ['y', 'n'], equals_to='n', default_option='y'):
                    quit()
                else:
                    continue

            value_type = type(query_col.find_one({key: {'$exists': True}})[key])
            if operator == '=':
                specified_mongo_query = {key: value_type(value)}
            elif operator == '<':
                specified_mongo_query = {key: {'$lt': value_type(value)}}
            else:
                specified_mongo_query = {key: {'$gt': value_type(value)}}

            if query_col.name == 'peptides':
                peptide_query.update(specified_mongo_query)
            else:
                file_info_query.update(specified_mongo_query)

    return peptide_query, file_info_query


def __peptide_info_query(peptides, peptide_query, ms1shape):
    peps = [f for f in peptides.find(peptide_query)]
    for f in peps:
        f.update({'file': f'{f["accession"]}/{f["Raw file"]}', 'ms1shape': ms1shape})

    return peps


def __file_info_query(peptides, peptide_query, class_col, file_info_query, file_info, f_class, peps):
    file_query_dict = {}
    raw_accession = list(np.unique([list(f.values()) for f in peptides.find(peptide_query, {'_id': 0, 'Raw file': 1, 'accession': 1})], axis=0))
    for f in raw_accession:
        if 'files' in class_col.name:
            file_info_query.update({'accession': f[0], 'Raw file': f[1], f_class: {'$exists': True}})
            info = file_info.find_one(file_info_query)
            file_query_dict[f'{f[0]}/{f[1]}'] = info[f_class] if info else False
        else:
            file_info_query.update({'accession': f[0], 'Raw file': f[1]})
            info = file_info.find_one(file_info_query)
            file_query_dict[f'{f[0]}/{f[1]}'] = True if info else False

    # Remove based on query
    peps = [f for f in peps if file_query_dict[f['file']]]
    # Add filtering class if class is from file info
    [f.update({f_class: file_query_dict[f['file']]}) for f in peps if 'files' in class_col.name]

    return peps