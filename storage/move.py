import shutil

from storage.db_init import __get_client_dbs
from utils.config_inits import get_image_path
from storage.serialization import __create_file


def __move_physical(project, from_db, to_db):
    path = get_image_path()
    __create_file(f'{path}{to_db}')
    print('Executing file transfer    ', end='\r')
    shutil.move(f'{path}{from_db}/{project}', f'{path}{to_db}')
    print('File transfer executed!    ')


def __move_db_entries(project, from_db_name, to_db_name):
    from_peptides = __get_client_dbs(from_db_name, 'peptides')[0]
    to_peptides = __get_client_dbs(to_db_name, 'peptides')[0]
    query = {'accession': project}
    data = list(from_peptides.find(query))
    [f.update({'_id': f['_id'].replace(from_db_name, to_db_name)}) for f in data]
    print('Executing database transfer   ', end='\r')
    to_peptides.insert_one(from_peptides.find_one({'_id': {'$ne': 'sizes'}}))
    to_peptides.insert_many(data)
    from_peptides.delete_many(query)
    print('Database transfer executed!   ')


def __move_data(project, from_db, to_db):
    print('Initializing file transfer', end='\r')
    __move_physical(project, from_db, to_db)
    print('Initializing database transfer', end='\r')
    __move_db_entries(project, from_db, to_db)