from time import sleep

from numpy import unique

from pride_api.common import __get_json, __get_link, __get_size
from pride_api.keys import __maxquant_file_names
from storage.compressions import __managed_exts, __loose_to_zip
from utils.common import match_ext
from utils.config_inits import get_sconfig


def __get_files_api(accession):
    return __get_json(get_sconfig()['pride_files'].replace('PXDxxxxxx', accession))


def __zip_filter(files_api):
    return match_ext(files_api['fileName'], ['.tar.gz']).lower() in __managed_exts() \
           and files_api['fileCategory']['value'] == 'SEARCH' \
           and files_api['fileSizeBytes']*1e-9 <= get_sconfig()['max_zipfile_size']


def __get_zip_files(api, get_size=False):
    if get_size:
        return [[__get_link(f), __get_size(f)] for f in api if __zip_filter(f)]
    else:
        return [__get_link(f) for f in api if __zip_filter(f)]


def __raw_filter(files_api):
    return match_ext(files_api['fileName']).lower() == '.raw' and files_api['fileCategory']['value'] == 'RAW'


def __get_raw_files(api, get_size=False):
    if get_size:
        return [[__get_link(f), __get_size(f)] for f in api if __raw_filter(f)]
    else:
        return [__get_link(f) for f in api if __raw_filter(f)]


def __get_loose_files(api, to_zip=False):
    mq_urls = [__get_link(f) for f in api for g in __maxquant_file_names() if f['fileName'] == g]
    xml_urls = [__get_link(f) for f in api if 'xml' in f['fileName']]
    loose_urls = mq_urls + xml_urls
    if not to_zip or not loose_urls:
        return loose_urls
    else:
        return __loose_to_zip(loose_urls)


def __get_extensions(api):
    filetypes = list(set([f.lower() for f in [match_ext(f['fileName'], ['.tar.gz']) for f in api]]))

    search_filetypes = list(set([f.lower() for f in [match_ext(f['fileName'], ['.tar.gz']) for f in api
                                                if f['fileCategory']['value'] == 'SEARCH']]))

    return {'filetypes': filetypes, 'search_filetypes': search_filetypes}


def __base_metadata_page(page_nr):
    return __get_json(f'https://www.ebi.ac.uk/pride/ws/archive/v2/projects?page={page_nr}&sortDirection=DESC&sortConditions=submissionDate')['_embedded']['projects']