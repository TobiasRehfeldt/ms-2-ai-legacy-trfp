import os
from urllib.request import urlretrieve

import pandas as pd

from extractor.create_images import __create_images
from extractor.mzml_extractor import __mzML_extractor
from extractor.mzml_formatter import __mzML_formatter
from extractor.mzml_metadata import __mzML_info
from storage.serialization import __create_file
from utils.config_inits import get_econfig
from utils.log import __internal_print, ms2ai_log


def __raw_file_downloader(filepath, rawfiles, filename, multiprocessing):
    __internal_print('Downloading raw file', multiprocessing)
    if not (os.path.exists(f'{filepath}file.raw') or os.path.exists(f'{filepath}file.mzML.gz') or os.path.exists(f'{filepath}mzML.txt')):
        for rawfiles in rawfiles:
            if filename in rawfiles:
                urlretrieve(rawfiles, f'{filepath}file.raw')
                break


def __raw_init(filepath, df, filename, rawfile_links, multiprocessing):
    # Create the path
    __create_file(filepath)
    # insert the MQ Dataframe
    if not os.path.exists(f'{filepath}{get_econfig()["maxquant_file"]}'):
        raw_df = df.loc[df['Raw file'] == filename, ]
        pd.DataFrame.to_csv(raw_df, f'{filepath}{get_econfig()["maxquant_file"]}', index=False)
    # download the raw file
    __raw_file_downloader(filepath, rawfile_links, filename, multiprocessing)


def __iterate_rawfiles(filename, filepath, accnr, rawfile_links, df, multiprocessing):
    try:
        __internal_print(f'file: {accnr}/{filename}', multiprocessing, False)
        if df is not None:
            __raw_init(filepath, df, filename, rawfile_links, multiprocessing)

        __mzML_formatter(accnr, filename, filepath, multiprocessing)
        __mzML_extractor(filepath, multiprocessing, accnr, filename)
        __mzML_info(filepath, filename, accnr, multiprocessing)
        if get_econfig()['extract_ms']:
            __create_images(accnr, filename, filepath, multiprocessing)
        __internal_print(f'{filename}: ✔', multiprocessing, False)

    except KeyboardInterrupt:
        quit()
    except Exception as e:
        ms2ai_log(f'{accnr}/{filename} (rawfile), {e.__class__.__name__}: {e}', 'info')
        pass