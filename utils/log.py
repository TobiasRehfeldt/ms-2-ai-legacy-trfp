import logging


def __internal_print(string, multiprocessing, overwrite=True):
    if not multiprocessing:
        if overwrite:
            print(f'{string}                                       ', end='\r')
        else:
            print(f'{string}                                       ')


def ms2ai_log(msg, level):
    logging.log(logging._checkLevel(level.upper()), msg)
