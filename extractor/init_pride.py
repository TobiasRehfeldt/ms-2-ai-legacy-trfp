import os
from glob import glob

from extractor.file_metadata import __summary_keys
from extractor.iter_zip import __pride_iter
from extractor.mzml_metadata import __mzML_keys
from extractor.xml_parser import __xml_keys, __xml_parser, __loose_xmls
from pride_api.get_urls import __get_zip_files, __get_raw_files, __get_loose_files, __get_files_api
from pride_api.keys import __pride_keys
from storage.db_init import __get_client_dbs
from utils.config_inits import get_econfig, __get_path, get_image_path
from storage.serialization import __create_file
from utils.log import __internal_print


def __check_pre_info(accnr, rawfiles):
    raw_dirs = [f for f in os.listdir(f'{__get_path()}{accnr}/') if not f.startswith('.')]

    if len(raw_dirs) == len(rawfiles):
        for files in raw_dirs:
            try:  # Attempt to calculate, if fails just return False
                # Get files in the directory, and check if the certain files exists.
                dir_files = os.listdir(f'{__get_path()}{accnr}/{files}/')
                mq_check = get_econfig()['maxquant_file'] in dir_files

                # Check if the raw, mzML or mzML.txt files are available
                file_check = any(i in ['file.raw', 'file.mzML.gz', 'mzML.txt'] for i in dir_files)
                # Check if the files databases have an entry, and the required informations.
                file_info_entries = __get_client_dbs('ms2ai', 'files')[0].find_one({'accession': accnr, 'Raw file': files})
                project_check = file_info_entries['source'] == 'pride' and any(item in file_info_entries.keys() for item in __pride_keys())
                summary_check = (file_info_entries['summary_exists'] and any(item in file_info_entries.keys() for item in __summary_keys())) or not file_info_entries['summary_exists']
                xml_check = (file_info_entries['xml_exists'] and all(item in file_info_entries.keys() for item in __xml_keys())) or not file_info_entries['xml_exists']
                mzML_check = all(item in file_info_entries.keys() for item in __mzML_keys())

                return mq_check and file_check and project_check and summary_check and xml_check and mzML_check and bool(file_info_entries)

            except:
                return False
    return False


def __check_for_skipability(haveallMQF, accnr, rawfiles):
    if haveallMQF and get_econfig()['acquire_only_new'] and len(rawfiles) == len(glob(f'{get_image_path()}{accnr}')):
        print(f'{accnr}: ✔ - {len(rawfiles)}/{len(rawfiles)} Rawfiles extracted')
        return True


def __check_complete_internal(accnr, all_raw):
    allCheck = [files for files in os.listdir(f'{__get_path()}{accnr}/') if not files.split('/')[-1].startswith('.') and
                'mzML.txt' in os.listdir(f'{__get_path()}{accnr}/{files}')]
    progress = '✓' if len(all_raw) == len(allCheck) else '✗' if len(allCheck) == 0 else '~'

    likely_culprit = '' if progress == '✓' else '[Likely culprit: FTP connection error, Corrupt zipfile, Rawfile mismatch, no Seqs/MS2 Scans, or too large zipfiles]' if progress == '✗' else '[Likely culprit: Rawfile mismatch, no Seqs/MS2 Scans, , or too large zipfiles]'

    print(f'{accnr} {progress}. Total files: {len(all_raw)}. Extracted files: {len(allCheck)}. {likely_culprit}')


def peptide_extractor(accnr, multiprocessing):
    __internal_print(f'\nAccessions: {accnr}', multiprocessing, False)
    __create_file(f'{__get_path()}{accnr}/')

    # Find all zipfiles, rawfiles, and loose MaxQuant files.
    files_api = __get_files_api(accnr)
    zipfiles, rawfiles = __get_zip_files(files_api), __get_raw_files(files_api)
    loose_files = __get_loose_files(files_api, to_zip=True)
    if loose_files:
        zipfiles.append(loose_files)

    # Check for all MaxQuant files and check for skipability
    pre_mq_info = __check_pre_info(accnr, rawfiles)
    if __check_for_skipability(pre_mq_info, accnr, rawfiles):
        return

    # Get loose XML files into 'files' db
    __xml_parser(__loose_xmls(accnr), accnr, [])

    # Go through all zipfiles
    for zips in zipfiles:
        __pride_iter(zips, pre_mq_info, accnr, rawfiles, multiprocessing)

    # Check to see how many files got extracted properly
    __check_complete_internal(accnr, rawfiles)