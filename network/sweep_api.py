from network.wandb_api import wandb_init
from utils.common import __validated_input, __urlretrieve_show_progress


def __sweep_config():
    sweep_config = {
        'method': 'random',
        'metric': {
            'name': 'val_accuracy',
            'goal': 'maximize'
        },
        'parameters': {
            'input_dropout': {
                'values': [0, 0.1, 0.2, 0.3, 0.4, 0.5]
            },
            'first_layer_neurons': {
                'values': [64, 128, 256, 512]
            },
            'hidden_layer_count': {
                'distribution': 'int_uniform',
                'min': 1,
                'max': 10
            },
            'hidden_layer_neurons': {
                'values': [64, 128, 256, 512]
            },
            # 'kernel_size': {
            #     'values': [2, 3, 5, 7, 10]
            # },
            # 'kernel_count': {
            #     'values': [2, 4, 8, 16]
            # },
            # 'L2_regularization': {
            #     'distribution': 'uniform',
            #     'min': 0,
            #     'max': 0.01
            # }
            # 'local_dropout': {
            #     'values': [0, 0.1, 0.2, 0.3, 0.4, 0.5]
            # },
            'learning_rate': {
                'distribution': 'uniform',
                'min': 0.0001,
                'max': 0.01
            },
            'decay_rate': {
                'values': [1, 0.9, 0.8, 0.7, 0.6, 0.5]
            },
            'epochs': {
                'values': [10, 15, 20]
            },
            'batch_size': {
                'values': [64, 128, 256, 512]
            },
            'early_stopping': {
                'values': [3, 4, 5, 6]
            },
            'optimizer': {
                'values': ['adam', 'rmsprop']
            },
        }
    }
    return sweep_config


def __wandb_sweep_train():
    import wandb
    from tensorflow.keras import optimizers
    from tensorflow.keras.models import Model
    from network.datagenerator import DataGenerator
    from network.get_partitions import get_partitions
    from tensorflow.keras.callbacks import EarlyStopping
    from wandb.integration.keras import WandbCallback
    from tensorflow.keras.optimizers.schedules import ExponentialDecay
    from tensorflow.keras.layers import Input, LocallyConnected2D, Flatten, Dense, Dropout, Conv2D

    config = wandb.init().config
    ms_level, test, checkup = ['ms1'], False, False
    params, partition, labels, test_accs, class_weight = get_partitions(ms_level, checkup)
    params['batch_size'] = config.batch_size
    training_generator = DataGenerator(partition['training'], labels['training'], params)
    validation_generator = DataGenerator(partition['testing'], labels['testing'], params)

    input_l = Input(shape=tuple(params['ms1shape']), name='ms1_input')
    x = Dropout(rate=config.input_dropout)(input_l)
    # x = LocallyConnected2D(config.kernel_count, kernel_size=(config.kernel_size, config.kernel_size), activation='relu',
    #                        kernel_regularizer=l2(l2=config.L2_regularization))(x)
    # x = Dropout(rate=config.local_dropout)(x)
    x = Flatten()(x)
    x = Dense(config.first_layer_neurons, activation='relu')(x)
    for f in range(config.hidden_layer_count):
        x = Dense(config.hidden_layer_neurons, activation='relu')(x)
    x = Dense(2, activation='softmax')(x)

    model = Model(inputs=input_l, outputs=x)
    opt_dict = {'sgd': optimizers.SGD, 'adam': optimizers.Adam, 'rmsprop': optimizers.RMSprop, 'adagrad': optimizers.Adagrad}
    learning_rate = ExponentialDecay(
        config.learning_rate,
        decay_steps=2000,
        decay_rate=config.decay_rate)
    model.compile(loss='categorical_crossentropy', metrics=['accuracy'], optimizer=opt_dict[config.optimizer](learning_rate=learning_rate))

    model.fit(x=training_generator, validation_data=validation_generator, epochs=config.epochs, class_weight=class_weight,
              callbacks=[WandbCallback(), EarlyStopping(monitor='val_accuracy', patience=config.early_stopping)])


def __fetch_model(sweep_id):
    from utils.config_inits import get_metadata_path
    import wandb
    from urllib.request import urlretrieve
    import os
    api = wandb.Api()
    team_name = wandb_init['team_name'] if wandb_init['team_name'] else wandb.api.default_entity
    try:
        runs = api.sweep(f"{team_name}/{wandb_init['project_name']}/{sweep_id}").runs
    except:
        quit('Cannot find sweep, make sure configuration file and sweep id is correct.')

    best_or_name = input('Input name of run or "best" for best run? ').lower()
    try:
        if best_or_name in ['best']:
            run = sorted(runs, key=lambda run: run.summary.get("val_accuracy", 0), reverse=True)[0]
        else:
            run = [f for f in runs if f.name == best_or_name][0]
    except:
        quit('Cannot find run, make sure to input the name from the run. example: atomic-red-10')

    if not run.state == 'finished':
        if __validated_input(f'Run state is "{run.state}". Want to download anyway?', ['y', 'n'], default_option='y', equals_to='n'):
            quit('Re-run to get another run!')
    f'https://api.wandb.ai/files/{team_name}/runs/{wandb_init["project_name"]}/{run.id}/model-best.h5'

    # urlretrieve(f'https://api.wandb.ai/files/{team_name}/{wandb_init["project_name"]}/{run.id}/model-best.h5', f"{get_metapath()}{run.name}.h5", __urlretrieve_show_progress)
    # run.file(f"model-best.h5").download(replace=True)
    run.file(f"model-best.h5").download(root=f"{get_metadata_path()}", replace=True)
    os.rename(f"{get_metadata_path()}model-best.h5", f"{get_metadata_path()}{run.name}.h5")
    print(f'{run.name}.h5 saved in the metadata folder!')


def __delete_model_files(sweep_id):
    import wandb

    api = wandb.Api()
    team_name = wandb_init['team_name'] if wandb_init['team_name'] else wandb.api.default_entity
    runs = api.sweep(f"{team_name}/{wandb_init['project_name']}/{sweep_id}").runs
    runs = sorted(runs, key=lambda run: run.summary.get("val_accuracy", 0), reverse=True)

    keep_amount = input(f'How many (of the best) models do you want to keep?')
    model_files = [g for f in runs[int(keep_amount):] for g in f.files() if 'h5' in g.name]
    [f.delete() for f in model_files]
    quit(f'\n{len(model_files)} models deleted, freeing up {round(sum([f.size * 1e-9 for f in model_files]), 1)} GB')


def __wandb_sweep(sweep_id=None, fetch=None, delete=None):
    import wandb

    if fetch:
        __fetch_model(fetch)

    if delete:
        __delete_model_files(delete)

    else:
        sweep_config = __sweep_config()
        if sweep_id is None:
            team_name = wandb_init['team_name'] if wandb_init['team_name'] else wandb.api.default_entity
            sweep_id = wandb.sweep(sweep_config, project=wandb_init['project_name'], entity=team_name)

        wandb.agent(sweep_id, project=wandb_init['project_name'], entity=wandb_init['team_name'], function=__wandb_sweep_train)
        quit('Sweep complete, ending!')

# api = wandb.Api()
# sweep = api.sweep(f"{team_name}/{wandb_init['project_name']}/{sweep_id}")
# runs = sweep.runs
# runs = sorted(runs, key=lambda run: run.summary.get('val_accuracy', 0), reverse=True)
# best_run = runs[0]
# artifacts = best_run.logged_artifacts()
# best_model = [artifact for artifact in artifacts if artifact.type == 'model'][0]