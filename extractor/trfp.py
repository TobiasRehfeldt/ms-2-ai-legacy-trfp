from sys import platform

from utils.common import __validated_input
from utils.config_inits import get_econfig
from utils.exceptions import condaInstallationError, dockerInstallationError
from utils.log import __internal_print


def __get_formatting_software(multiprocessing):
    config_check = get_econfig()['format_software'] == 'conda'
    platform_check = (platform.startswith('linux') or platform.startswith('darwin'))
    if config_check and platform_check:
        return 'conda'
    else:
        if get_econfig()['format_software'] == 'conda':
            __internal_print('Formatting software changed to Docker due to operating system incompatibilities', multiprocessing, overwrite=False)
        return 'docker'


def __install_conda_thermo(channel_list=None):
    if channel_list is None:
        channel_list = ['defaults', 'bioconda', 'conda-forge']

    try:
        from conda.cli.python_api import run_command, Commands

        if not 'thermorawfileparser' in str(run_command(Commands.LIST)):
            if __validated_input(f'Want to ThermoRawFileParser for Docker?', ['y', 'n'], default_option='y', equals_to='n'):
                quit(f'Cannot run MS2AI extractor without a formatting software.')

            [run_command(Commands.CONFIG, '--add', 'channels', x) for x in channel_list]
            run_command(Commands.INSTALL, '-c', 'bioconda', 'thermorawfileparser')

    except ModuleNotFoundError:
        raise condaInstallationError

    except Exception as e:
        print(f'Conda error: {e}')


def __install_docker_thermo():
    try:
        import docker
        from docker.errors import DockerException

        client = docker.from_env()
        if not 'veitveit/trp:latest' in [f.tags[0] for f in client.images.list()]:
            if __validated_input(f'Want to ThermoRawFileParser for Docker?', ['y', 'n'], default_option='y', equals_to='n'):
                quit(f'Cannot run MS2AI extractor without a formatting software.')

            client.images.pull('veitveit/trp')
    except ModuleNotFoundError or DockerException:
        raise dockerInstallationError

    except Exception as e:
        print(f'Docker error: {e}')


def __prepare_formatting_software(multiprocessing):
    software = __get_formatting_software(multiprocessing)
    if software == 'conda':
        __install_conda_thermo()

    elif software == 'docker':
        __install_docker_thermo()