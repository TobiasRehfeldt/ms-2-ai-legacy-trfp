import re
from multiprocessing.pool import Pool

from scraper.base_metadata import __metadata_base
from scraper.file_metadata import __metadata_files
from storage.db_init import __get_client_dbs, __insert_or_update
from utils.common import __validated_input
from utils.config_inits import get_sconfig


def __remove_error(pride):
    # Unknown error drops!
    unknown_count = pride.count_documents({'maxquant_files': 'error: unknown zipfile error', 'source': 'pride'})
    pride.delete_many({'maxquant_files': 'error: unknown zipfile error', 'source': 'pride'})

    # Size error drops!
    def __size_error_index(entry, i):
        return re.search('\d+', entry['maxquant_files']).span()[i]

    size_error = list(pride.find({'maxquant_files': {'$regex': 'error:.+\d+'}, 'source': 'pride'}))
    size_error = [f for f in size_error if int(f['maxquant_files'][__size_error_index(f, 0):__size_error_index(f, 1)]) < get_sconfig()["max_zipfile_size"]]
    [pride.delete_one(f) for f in size_error]

    # pride.find({'maxquant_files': 'error: unknown zipfile error'})
    print(f"Project entries with size errors removed: {len(size_error)}\n"
          f"Project entries with unknown errors removed: {unknown_count}")


def __scraper_init():
    projects = __get_client_dbs('ms2ai', 'projects')[0]

    if projects.count_documents({'source': 'pride'}) != 0 and __validated_input('Overwrite current project metadata originating from PRIDE?', ['y', 'n'], equals_to='y', default_option='n'):
        projects.delete_many({'source': 'pride'})
    elif projects.count_documents({'source': 'pride'}) != 0 and __validated_input('Overwrite metadata entries with errors?', ['y', 'n'], equals_to='y', default_option='n'):
        __remove_error(projects)

    return projects


def run_scraper():
    pride = __scraper_init()

    # Pull the base metadata from pride API
    metadata = [{**f, **{'source': 'pride'}} for f in __metadata_base()]
    existing = pride.distinct('accession')
    metadata = [f for f in metadata if f['accession'] not in existing]
    print(f'{len(metadata)} new entries!')
    # metadata = [{'accession': 'PXD026828', 'projectDescription': 'it has maxquant!', 'purpose': 'testing'}]

    # Multi-processed
    if get_sconfig()['multiprocessing']['apply'] and len(metadata) > 1:
        with Pool(min(get_sconfig()['multiprocessing']['processes'], len(metadata))) as ps:
            for i, pride_results in enumerate(ps.imap_unordered(__metadata_files, metadata)):
                print(f'Pulling file metadata: {i} / {len(metadata)}    ', end='\r')
                if pride_results is not None:
                    __insert_or_update(pride, pride_results, ['accession'])
    # Single-threaded
    else:
        for i, meta in enumerate(metadata):
            print(f'Pulling file metadata: {i} / {len(metadata)}    ', end='\r')
            pride_results = __metadata_files(meta)
            if pride_results is not None:
                __insert_or_update(pride, pride_results, ['accession'])

    print('File metadata pulled!                   ')

# For debugging:
# Loose XML files: PXD017125
# zip XML files: PXD026828, PXD011164
# Loose MQ files: PXD023821
# Loose MQ files and xml file: PXD023540
# .zip: PXD027822
# .zip > 100gb: PXD022950
# .zip > 5gb: PXD025315
# .tar.gz: PXD001426
# .7z: PXD024198
# .rar: PXD023650
# [f for f in metadata if f['accession'] == 'PXD018154']