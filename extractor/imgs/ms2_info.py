import numpy as np
import pandas as pd

from extractor.common import __get_lower_bound
from utils.config_inits import get_econfig


def __get_info_lists(ms2_mzML, ms2scan, noise_removal=0.05):
    intensity_array = ms2_mzML[str(ms2scan)]['intensity']
    noise_index = np.array(intensity_array) > (noise_removal * max(intensity_array))

    mz_array = np.array(ms2_mzML[str(ms2scan)]['m/z'])[noise_index]
    int_array = np.array(intensity_array)[noise_index]

    return mz_array, int_array, max(intensity_array)


def __ms2_numerical_bin(mz_list, int_list, binning):
    ms_info = [[0]] * binning['bin_amount']
    ratio = binning['fixed_mz_length'] / binning['bin_amount']
    for mz, ints in zip(mz_list, int_list):
        indx = np.int(min(mz / ratio, binning['bin_amount'] - 1))
        if ms_info[indx] == [0]:
            ms_info[indx] = [ints]
        else:
            ms_info[indx].append(ints)
    max_int = max(int_list)
    return [np.mean(f) / max_int for f in ms_info]


def __ms2_bin(ms2_mzML, ms2scan, ms2_binning):
    mz_list, int_list, max_int = __get_info_lists(ms2_mzML, ms2scan)
    end_range = ms2_binning['fixed_mz_length'] if ms2_binning['fixed_mz'] else max(mz_list)
    hist = np.histogram(mz_list, bins=ms2_binning['bin_amount'], range=(0, end_range))

    # With intensity. Means all intensities and normalizes intensities of the ms2 spectrum
    if ms2_binning['retain_int']:
        ms2_info = __ms2_numerical_bin(mz_list, int_list, ms2_binning)
    else:  # Without intensity
        ms2_info = np.array(hist[0] > 0, dtype=np.int32)

    ms2_info = np.array([ms2_info, hist[0]])
    ms2_size = ms2_info.shape

    return ms2_info, ms2_size


def __find_ms2_scan(rows, mzmlfile, scan_name):
    if ';' not in str(rows[scan_name]):
        return int(rows[scan_name])

    else:
        # Retrieve ms2data from mzml file
        ms2list = [[f, mzmlfile['ms2'][str(f)]['precursor_mz'] - rows['m/z']] for f in
                   [int(scans) for scans in rows[scan_name].split(';')] if len(mzmlfile['ms2'][str(f)]['intensity']) > 0]

        return sorted(ms2list, key=lambda x: x[1])[0][0]


def __ms2_scan_name(mq_df):
    return [f for f in mq_df.columns if 'scan number' in f.lower()][0]
    # pepfile = get_econfig()['maxquant_file']
    #
    # if pepfile == 'allPeptides.txt':
    #     return 'MSMS Scan Numbers'
    #
    # elif pepfile == 'evidence.txt':
    #         return 'MS/MS Scan Number'
    #
    # elif pepfile == 'msms.txt':
    #     return 'Scan number'