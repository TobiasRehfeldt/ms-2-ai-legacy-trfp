from io import BytesIO
from zipfile import *

from storage.compression_handlers import __zip_handler
from utils.common import match_ext, __url_open_attempts
from utils.log import ms2ai_log


def __loose_to_zip(urls):
    project = [f for f in urls[0].split('/') if 'PXD' in f][0]
    try:
        data = [__url_open_attempts(f) for f in urls]
        zip_elem = ZipFile(file=BytesIO(), mode='w')
        for name, elem in zip(urls, data):
            zip_elem.writestr(name, elem)

        return zip_elem

    except Exception as e:
        ms2ai_log(f'{project}: {e}', 'info')


def __get_compressed_file(url):
    zip_compressions = {'.zip': __zip_handler}# , '.tar.gz': __tar_handler, '.7z': __7z_handler, 'rar': __rar_handler, '.gz': __gzip_handler}
    return zip_compressions[match_ext(url, ['.tar.gz'])](url)


def __ext_from_zip(file_list, url=None, zip_file=None):
    if bool(url):
        zip_file = __get_compressed_file(url)

    return [[zip_file.open(file) for file in zip_file.filelist if name in file.orig_filename and 'MACOSX' not in file.orig_filename] for name in file_list]


def __managed_exts():
    return ['.zip']  # , '.tar.gz' , '.7z', '.rar', '.gz', '.gzip'



