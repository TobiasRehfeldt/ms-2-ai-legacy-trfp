import os
from math import ceil

import numpy as np

from extractor.imgs.create_fulltxt import __create_image_data
from extractor.imgs.create_peptxt import __pep_txt_image
from extractor.imgs.create_pngs import __full_png_image
from storage.db_init import __get_client_dbs
from storage.serialization import read, write
from utils.config_inits import get_econfig
from utils.log import __internal_print


def __image_preparameters(filename, accnr, multiprocessing):
    __internal_print('Preparing parameter for image creation', multiprocessing)
    file = __get_client_dbs('ms2ai', 'files')[0].find_one({'identifier': f'{accnr}/{filename}'})

    (mz_bin, rt_bin) = (get_econfig()['mz_bin'], get_econfig()['rt_bin'] if not get_econfig()['rt_as_percentage'] else \
                       (file['last pep rt'] - file['first pep rt']) * (get_econfig()['rt_bin'] / 100))

    windows = [int(round(get_econfig()['mz_interval'] / mz_bin, 0)),
              int(round(get_econfig()['rt_interval'] / rt_bin, 0)) if not get_econfig()['rt_as_percentage'] else \
              int(round(((file['last pep rt'] - file['first pep rt']) * (get_econfig()['rt_interval'] / 100)) / rt_bin, 0))]

    # These will be one longer than the resolution, as the bins are between the values.
    range_lists = [np.arange(int(file['ms1 min m/z']), ceil(file['ms1 max m/z']) + mz_bin, mz_bin).tolist(),
                   np.arange(int(file['min rt']), ceil(file['max rt']) + rt_bin, rt_bin).tolist()]

    resolution = {'x': int(round((int(file['ms1 max m/z']) - int(file['ms1 min m/z'])) / mz_bin, 0)),
                  'y': int(round((int(file['max rt']) - int(file['min rt'])) / rt_bin, 0))}

    return windows, range_lists, resolution


def __create_images(accnr, filename, filepath, multiprocessing):
    mzML = read(f'{filepath}mzML.txt')
    window, range_lists, resolution = __image_preparameters(filename, accnr, multiprocessing)
    if not os.path.exists(f'{filepath}{resolution["x"]}x{resolution["y"]}.txt'):
        image = __create_image_data(mzML['ms1'], filename, accnr, resolution, range_lists, multiprocessing)
        if get_econfig()['save_images_as_png']:
            __full_png_image(image[::-1], filepath, resolution, filename, accnr)
        __internal_print('Saving image files', multiprocessing)
        write(image, f'{filepath}{str(resolution["x"])}x{str(resolution["y"])}.txt')

    else:
        image = read(f'{filepath}{resolution["x"]}x{resolution["y"]}.txt')
    __pep_txt_image(image, mzML['ms2'], filepath, accnr, filename, window, range_lists, multiprocessing)
