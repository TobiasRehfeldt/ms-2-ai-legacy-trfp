import tarfile
from io import BytesIO
from urllib.request import urlretrieve
from zipfile import ZipFile


from storage.serialization import __create_file
from utils.common import __url_open_attempts
from utils.config_inits import __get_path


def __zip_handler(url):
    data = __url_open_attempts(url)
    return ZipFile(BytesIO(data))


def __tar_handler(url):
    data = __url_open_attempts(url)
    tar_file = tarfile.open(fileobj=BytesIO(data), mode='r:gz')
    zip_elem = ZipFile(file=BytesIO(), mode='w')
    for file_name in tar_file.getnames():
        if file_name.endswith(('.xml', '.txt')):
            zip_elem.writestr(file_name, tar_file.extractfile(file_name).read())
    return zip_elem


# def __7z_handler(url):
#     import py7zr
#
#     data = __url_open_attempts(url)
#     svn_zip = py7zr.SevenZipFile(BytesIO(data)).readall()
#     zip_elem = ZipFile(file=BytesIO(), mode='w')
#     for f in svn_zip:
#         zip_elem.writestr(f, svn_zip[f].read())
#     return zip_elem


# def __rar_handler(url):
#     import rarfile
#
#     zipfile_name = '_'.join(url.split("/")[-2:])
#     tmp_path = __get_path() + 'tmp/'
#     __create_file(tmp_path)
#     urlretrieve(url, f'{tmp_path}{zipfile_name}')
#     rar_file = rarfile.RarFile(f'{tmp_path}{zipfile_name}')
#     zip_elem = ZipFile(file=BytesIO(), mode='w')
#     for file_name in rar_file.namelist():
#         if file_name.endswith(('.xml', '.txt')):
#             zip_elem.writestr(file_name, rar_file.open(file_name).read())
#     return zip_elem


def __gzip_handler(url):
    print()