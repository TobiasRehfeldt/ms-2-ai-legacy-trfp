import os
from sys import platform

from utils.common import __validated_input
from utils.config_inits import get_sconfig, get_metadata_path
from google_drive_downloader import GoogleDriveDownloader as gdd


def __get_client_dbs(db_name, *args):
    if db_name is None:
        db_name = get_sconfig()['db_name']

    from pymongo import MongoClient
    client = MongoClient()
    db = client[db_name]

    cols = [db[str(f)] for f in args]
    cols.append(db)
    cols.append(client)

    return cols


def __get_current_pride():
    if __get_client_dbs('ms2ai', 'projects')[0].find_one() and __validated_input('Overwrite current project metadata originating from PRIDE?', ['y', 'n'], equals_to='y', default_option='n'):
        __get_client_dbs('ms2ai', 'projects')[0].delete_many({'source': 'pride'})

    try:
        print(f'Downloading current version of the projects collection!')
        gdd.download_file_from_google_drive(file_id='1IVhDo7vo6TI6N8Nmid7oljjnIcYyoS85', dest_path=f'{get_metadata_path()}projects.bson.gz')
        os.system(f'mongorestore{"" if platform.startswith("linux") or platform.startswith("darwin") else ".exe"} --gzip --archive="{get_metadata_path()}projects.bson.gz" --nsInclude="ms2ai.projects"')
        os.remove(f'{get_metadata_path()}projects.bson.gz')
        print(f'"projects" collection has successfully  been imported!')

    except:
        print(f'Failed to import the current projects collections'
              f'\nMake sure that MongoDB is running, and (for windows) that the MongoTools are downloaded and added to system paths')
        quit()


def __insert_or_update(col, insert, *query):
    if isinstance(query[0], dict):
        query = query[0]
    else:
        query = dict([(f, insert[f]) for f in query[0]])
    if bool(col.find_one(query)):
        col.update_one(query, {'$set': insert})
    else:
        col.insert_one(insert)


# mongodump --archive=pride.bson.gz --gzip --db ms2ai --collection pride
# MongoClient().list_database_names()
# MongoClient()['db'].list_collection_names()
# MongoClient().drop_database('db')