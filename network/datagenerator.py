from time import time

import numpy as np
from tensorflow.keras.utils import Sequence, to_categorical

from storage.serialization import read
from utils.config_inits import get_image_path, get_nconfig
from utils.log import ms2ai_log


class DataGenerator(Sequence):
    'Generates data for Keras'

    def __init__(self, list_IDs, labels, params):
        'Initialization'
        self.params = params
        self.list_IDs = list_IDs
        self.labels = labels
        self.on_epoch_end()
        self.n = 0
        self.max = self.__len__()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.params['batch_size']))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index * self.params['batch_size']:(index + 1) * self.params['batch_size']]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]
        # Generate data
        X, y = self.__data_generation(list_IDs_temp)
        return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.params['shuffle']:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples'
        # BRING IN BATCH DATA
        start = time()
        batch = [[read(f'{get_image_path()}{ID}.txt'), self.labels[ID]] for ID in list_IDs_temp]
        end = time()
        # print(f' Batch load time: {end - start}')

        # Initialize empty sequences
        y = np.zeros(self.params['batch_size'])

        default_inputs = {
            'ms1': np.zeros(tuple([self.params['batch_size']] + self.params['ms1shape'])),
            'ms1+': np.zeros((self.params['batch_size'], self.params['ms1_add'] if self.params['ms1_add'] else 0)),
            'ms2': np.zeros(tuple([self.params['batch_size']] + self.params['ms2shape'])),
            'ms2+': np.zeros((self.params['batch_size'], self.params['ms2_add'] if self.params['ms2_add'] else 0))
        }

        data_input = {}
        for f in self.params['ms_level']:
            ms = f.split('+')[0]
            for g in f.split('+'):
                data_input[f'{ms}{"+" if not g else ""}'] = default_inputs[f'{ms}{"+" if not g else ""}']

        # Fill empty sequences with batch data
        for i, peptide in enumerate(batch):
            data = {
                'ms1': peptide[0][0][:, :, self.params['channels']],
                'ms1+': np.array(peptide[0][2]),
                'ms2': peptide[0][1][:, np.argpartition(peptide[0][1][1], len(peptide[0][1][1]) - self.params['ms2shape'][1])[-self.params['ms2shape'][1]:]],
                'ms2+': np.array(peptide[0][3]),
                'label': peptide[1]
            }

            for f in self.params['ms_level']:
                ms = f.split('+')[0]
                for g in f.split('+'):
                    try:
                        data_input[f'{ms}{"+" if not g else ""}'][i,] = data[f'{ms}{"+" if not g else ""}']
                    except:
                        print(list_IDs_temp[i])

            y[i] = data['label']

        if self.params['classification']:
            try:
                y = to_categorical(y, num_classes=self.params['n_classes'])
            except:
                quit('Error in attempting one-hot encoding. Most likely batch_size or test_percentage issue')

        return (list(data_input.values()), y)

    def __next__(self):
        if self.n >= self.max:
           self.n = 0
        result = self.__getitem__(self.n)
        self.n += 1
        return result