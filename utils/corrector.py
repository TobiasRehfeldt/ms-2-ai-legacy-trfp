import os
import sys
from glob import glob
from multiprocessing import Pool

from simplejson import loads

from storage.db_init import __get_client_dbs
from storage.serialization import read, write
from utils.config_inits import __get_path


def mzML_corrector(file):
    try:
        identifier = file.replace('\\', '/').split('/', 2)[-1].rsplit('/', 1)[0]
        data = read(file)
        ms2_vals = [[min(data['ms2'][f]['m/z']), max(data['ms2'][f]['m/z']), max(data['ms2'][f]['intensity'])] for f in data['ms2']]
        min_ms2_mz, max_ms2_mz, max_ms2_int = min([f[0] for f in ms2_vals]), max([f[1] for f in ms2_vals]), max([f[2] for f in ms2_vals])
        data['file']['ms2'] = {'min m/z': min_ms2_mz, 'max m/z': max_ms2_mz, 'max intensity': max_ms2_int}
        db_insert = {'ms2 min m/z': min_ms2_mz, 'ms2 max m/z': max_ms2_mz, 'ms2 max intensity': max_ms2_int}
        __get_client_dbs('ms2ai', 'files')[0].update_one({'identifier': identifier}, {'$set': db_insert})
        write(data, file)
    except:
        os.remove(file)


def mzML_init():
    path = loads(open(sys.argv[0].rsplit('/', 2)[0] + '/config.json').read())['storage']['path']
    mp = loads(open(sys.argv[0].rsplit('/', 2)[0] + '/config.json').read())['storage']['multiprocessing']
    mzmls = glob(f'{path}/PXD*/*/mzML.txt')
    if mp['apply']:
        with Pool(min(len(mzmls), mp['processes'])) as ps:
            for i, _ in enumerate(ps.imap_unordered(mzML_corrector, mzmls)):
                print(f'mzMLs corrected: {i}/{len(mzmls)}', end='\r')
    else:
        for i, f in enumerate(mzmls):
            mzML_corrector(f)
            print(f'mzMLs corrected: {i}/{len(mzmls)}', end='\r')


if __name__ == '__main__':
    script = sys.argv[1]
    if script == 'mzML.txt':
        mzML_init()